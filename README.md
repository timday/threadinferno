threadinferno
=============

An update of older cpuinferno project.

![Use all the cores!](http://i.imgur.com/A85s7I4.jpg)

* [Results](results/README.md)
* [Analysis](analyse/ANALYSIS.md)

TODO
----
* Analyse; heatmaps of job interference.
* TBB docs disagree re `wait_until_empty()` (expected) vs `debug_wait_until_empty()` (actual).
* Figure out better way to set thread priorities?
    * According to `http://www.threadingbuildingblocks.org/sites/default/files/resources/CHANGES.txt`, "Removed task_scheduler_observer::on_scheduler_leaving() callback." in TBB 4.2 update 4.  So have to do everything via `on_scheduler_entry` / `on_scheduler_exit`, which means moving priority both ways.
    * Either run as root, or in theory can use `setcap` to enable CAP_SYS_RESOURCE on an executable which will let it adjust its RLIMIT_NICE.

Links
-----
* TBB arenas & prioritization thread on Intel forums: https://software.intel.com/en-us/forums/topic/519864

TBB version
-----------
Needs TBB 4.2 for arenas.
Wheezy has 4.0.
In a dir:

* Temporarily add jessie repos to `/etc/apt/sources.list`
* `sudo apt-get build-deb tbb` - never actually seen this do anything
* `sudo apt-get -t jessie -b source tbb`
* sudo `dpkg -i *.deb`
* Revert `/etc/apt/sources.list`

But that's not as up to date as update 5; so in the end just used the .tgz from TBB downloads.

AWS EC2
-------

* Testing with 8 vcores: candidates c1.xlarge, m2.4xlarge, c3.2xlarge maybe covers 3 generations of HW ?  Or maybe not, c1.xlarge seems to be recent Xeon models (but not fast).
* Debian Wheezy image links at <https://wiki.debian.org/Cloud/AmazonEC2Image/Wheezy>.  NB Add instance storage to have it mountable.
* Fire up a spot instance with wheezy AMI from link above.  NB these are login "admin".
* Login ssh -v -i ...pathtokey... admin@xxx.xxx.xxx.xxx
* `sudo aptitude update`
* `sudo aptitude full-upgrade`
* `sudo shutdown -r +1 &` and login again...
* Make instance storage available:
    * `sudo /sbin/mkfs.ext4 /dev/xvdb`
    * `sudo /sbin/mkfs.ext4 /dev/xvdc`
    * `sudo mkdir /mnt/ephemeral0 /mnt/ephemeral1`
    * `sudo mount /dev/xvdb /mnt/ephemeral0`
    * `sudo mount /dev/xvdc /mnt/ephemeral1`
    * `df -h`
* `sudo aptitude install g++ make mercurial less emacs time libboost-dev libboost-program-options-dev`
* `( mkdir -p download/tbb && cd download/tbb && wget http://www.threadingbuildingblocks.org/sites/default/files/software_releases/linux/tbb43_20140724oss_lin.tgz && tar xvfz tbb43_20140724oss_lin.tgz )`
* `mkdir project && cd project`
* `hg clone https://timday@bitbucket.org/timday/threadinferno threadinferno`
* `cd threadinferno`
* `WORKSPACE=/mnt/ephemeral0 make` or `WORKSPACE=/mnt/ephemeral0 make > results/output-... &` then `tail -f results/output-...`
* Capture results in results.  Update <results/README.md>.  Don't forget to hg add, commit, push results.

Other relevant stuff:

* Info on configuring instance stores [here](http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/InstanceStorage.html)

Windows
-------
Project properties need changing for building for Server2003 or Server2012.

* General - Platform Toolset - gives an XP or not option
* C++ - Preprocessor - WINVER=0x0502 for Server2003, or 0x0600 for Vista/Server2008+.

Tests run with files on `z:` instance storage.

HugePages
---------

Assuming system is setup for them already:
* In /etc/sysctl.conf: `vm.nr_hugepages = 3072` for 6GByte HugePages available on reboot or `sync ; echo 3 > /proc/sys/vm/drop_caches ; sysctl -p` check with `cat /proc/meminfo`
* Run executable with LD_PRELOAD=libhugetlbfs.so LD_LIBRARY_PATH=~/project/cpuinferno/hugetlbfs/libhugetlbfs-2.17/obj64 HUGETLB_MORECORE=yes

On an EC2 hvm:
* libhugetlbfs is in `hugetlbfs/SETUP`.  Will also need to install `libc6-dev-i386` dependency.

* As root:
    groupadd hugetlbfs
    getent group hugetlbfs
    adduser admin hugetlbfs
    cat <<EOF >> /etc/sysctl.conf
    vm.nr_hugepages = 3072
    vm.hugetlb_shm_group = 1001
    EOF
    mkdir /hugepages
    cat <<EOF >> /etc/fstab
    hugetlbfs /hugepages hugetlbfs mode=1770,gid=1001 0 0
    EOF
    sync ; echo 3 > /proc/sys/vm/drop_caches
    sysctl -p
    cat /proc/meminfo
    mount /hugepages
