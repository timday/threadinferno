#include "aborter.h"

#ifdef WIN32
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
void sleep(int s) {Sleep(1000*s);}
#else
#include <unistd.h>
#endif

#include "tbb_prioritizer.h"

aborter::aborter()
  :_flag(false)
{}

aborter::aborter(int delay)
  :_flag(false)
  ,_thread(
    [this,delay]()->void {
      set_worker_thread_priority_high();
      sleep(delay);
      fire();
    }
  )
{}

aborter::~aborter() {
  if (_thread.joinable()) {
    _thread.join();
  }
}

void aborter::fire() {
  _flag.store(true,std::memory_order_relaxed);
}
