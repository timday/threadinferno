#ifndef __threadinferno_aborter_h__
#define __threadinferno_aborter_h__

#include <atomic>
#include <thread>

struct aborter {
  aborter(); // Never aborts
  explicit aborter(int delay);  

  ~aborter();
  
  operator bool() const {
    return _flag.load(std::memory_order_relaxed);
  }

private:
  void fire();
  std::atomic<bool> _flag;
  std::thread _thread;
};

#endif
