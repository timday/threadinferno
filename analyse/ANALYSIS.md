Analysis
========

Of results [here](../results/README.md).

Gross timings statistics
------------------------
Inspecting the outputs, it's noticeble how much more variation the is in test timings on windows generally.
This is probably mainly due to the OS differences in scheduler timeslicing (16ms Windows, 1ms Linux?).
Statistics for aggregated times in seconds for all test runs:

    Windows                                 :       Mean         SD        Min        Max
    ec2-c1.xlarge-2003r2-winver0x0502       :      5.006      0.275      4.947     15.223
    ec2-c1.xlarge-2012r2-winver0x0502       :      5.016      0.051      5.000      6.844
    ec2-c1.xlarge-2012r2                    :      5.015      0.048      5.000      6.774
    ec2-c3.2xlarge-2003r2-winver0x0502      :      5.000      0.131      4.985      9.858
    ec2-c3.2xlarge-2012r2-winver0x0502      :      5.015      0.122      5.000      9.546
    ec2-c3.2xlarge-2012r2                   :      5.016      0.102      5.000      7.727
    
    Linux                                   :       Mean         SD        Min        Max
    ec2-c1.xlarge-wheezy                    :      5.005      0.010      5.000      5.075
    ec2-c3.2xlarge-wheezy                   :      5.002      0.005      5.000      5.049
    native-namche_i7-wheezy-vbox            :      5.001      0.002      5.000      5.043
    native-namche_i7-wheezy                 :      5.001      0.005      5.000      5.050

The high max values seem to arise from a small number of random outages per run (no obvious pattern).
Interesting that Linux doesn't seem to be affected at all
(which suggests it's not an EC2 phenomenon, although more linux test runs would give more confidence).
All EC2 tests run on a Sunday morning on EU-West.

* [Windows test timing histogram](timing-windows.png)
* [Linux test timing histogram](timing-linux.png)

TBB `blocked_range2d` access pattern
------------------------------------
Just out of curiosity: plots of what order TBB starts tasks in using `parallel_for` over a `blocked_range2d` and how they are processed by worker threads.
Results are different each time (but qualitiatively similar).

Default minimum grain size (1):

* [Task ordering](blocked_range2d-1.png)
* [Task assignment to thread](blocked_range2d-1-tid.png) - note the spatial coherence (tasks close together generally run on the same thread).

And the same but with (minimum) grain size 32 (both dimensions).

* [Task ordering](blocked_range2d-32.png)
* [Task assignment to thread](blocked_range2d-32-tid.png)

Inter-thread interference
-------------------------

TODO
