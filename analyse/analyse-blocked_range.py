#!/usr/bin/env python

import matplotlib
matplotlib.use('Agg')    
import matplotlib.pyplot as plt
import numpy as np

def analyse(grainsize,filenamein,filenameoutbase):
    
    img=np.zeros((1024,1024))
    timg=np.zeros((1024,1024))
    mask=np.ones((1024,1024,4))
    c=0
    i=0
    t=1
    tids={}
    annotations=[]
    for line in file(filenamein):
        if len(line)>0 and line[0]!='>':
            fields=[x.strip() for x in line.split()]
            oy=int(fields[0])
            ox=int(fields[1])
            sy=int(fields[2])
            sx=int(fields[3])
            tid=fields[4]
            img[oy:oy+sy,ox:ox+sx]=c
            if tid not in tids:
                tids[tid]=t
                t=t+1
            timg[oy:oy+sy,ox:ox+sx]=tids[tid]
            mask[oy:oy+sy,ox     ,0:3]=0.0
            mask[oy:oy+sy,ox+sx-1,0:3]=0.0
            mask[oy     ,ox:ox+sx,0:3]=0.0
            mask[oy+sy-1,ox:ox+sx,0:3]=0.0
            c=c+sx*sy
            i=i+1
            annotations.append((ox+sx/2,oy+sy/2,str(i),12.0*min(sx,sy)/16.0))

    print '{0}: found {1} thread IDs'.format(filenamein,len(tids))

    cmap=plt.cm.rainbow # or hsv
    cimg=cmap(img/np.amax(img))
    cimg=mask*cimg
    
    plt.figure(figsize=(40,40))

    plt.imshow(cimg,interpolation='none',cmap=cmap)
    plt.colorbar(ticks=[],shrink=0.75,orientation='horizontal',pad=0.05)

    for a in annotations:
        plt.text(a[0],a[1],a[2],ha='center',va='center',fontsize=str(a[3]))
    
    plt.gca().get_xaxis().set_visible(False)
    plt.gca().get_yaxis().set_visible(False)

    plt.title('TBB blocked_range2d traversal order\n8 threads on 1024x1024 range, grainsize '+str(grainsize)+'\n',fontsize=48)

    plt.savefig(filenameoutbase+'.png')
    plt.savefig(filenameoutbase+'.pdf')

    plt.figure(figsize=(40,40))

    cmap=plt.cm.get_cmap('rainbow',len(tids)) # or 'jet'
    plt.imshow(timg,interpolation='none',cmap=cmap)
    cb=plt.colorbar(shrink=0.75,orientation='horizontal',pad=0.05)
    cbticks=[1+(t-1)*(0.5+x-1)/t for x in xrange(1,t)]
    cblabels=[str(x) for x in xrange(1,t)]
    cb.set_ticks(cbticks)
    cb.set_ticklabels(cblabels)

    for a in annotations:
        plt.text(a[0],a[1],a[2],ha='center',va='center',fontsize=str(a[3]))
    
    plt.gca().get_xaxis().set_visible(False)
    plt.gca().get_yaxis().set_visible(False)

    plt.title('TBB blocked_range2d task-thread assignment\n8 threads on 1024x1024 range, grainsize '+str(grainsize)+'\n',fontsize=48)

    plt.savefig(filenameoutbase+'-tid.png')
    plt.savefig(filenameoutbase+'-tid.pdf')
    
def main():
    analyse(1,'../results/blocked_range2d-1.txt','blocked_range2d-1')
    analyse(32,'../results/blocked_range2d-32.txt','blocked_range2d-32')

if __name__ == '__main__':
    main()

