#!/usr/bin/env python

from collections import defaultdict
import glob
import matplotlib
matplotlib.use('Agg')    
import matplotlib.pyplot as plt
import numpy as np
import os
import scipy.stats

def analyse(filename):

    experimentname=filename.split('/')[-1].replace('output-','').replace('.txt','')

    print filename
    plt.figure(figsize=(36,36))
    plt.suptitle(experimentname,fontsize=36)

    plt.tick_params(axis='both',which='major',labelsize=6)
    plt.tick_params(axis='both',which='minor',labelsize=6)

    jobnames=[]
    timings=[defaultdict(dict),defaultdict(dict),defaultdict(dict)]

    i=0
    for line in file(filename):
        if len(line)>0 and line[0]=='>':
            i=i+1
        elif i==1 and len(line)>0 and line[0]=='$':
            jobnames.append(line[1:].split(':')[0].strip())
        elif i>3 and len(line)>0 and line[0]=='$':
            fields=map(lambda x: x.strip(),line.split(':'))
            job0=fields[0][1:].split('&')[0].strip()
            job1=fields[0][1:].split('&')[1].strip()
            score0=float(fields[2].split('&')[0].strip())
            score1=float(fields[2].split('&')[1].strip())
            score=float(fields[3].strip())
            timings[i-4][job0][job1]=(score0,score1,score)

    s=(len(jobnames),len(jobnames))
    
    rowtitles={
        0:'Non-oversubscribed',
        1:'Oversubscribed',
        2:'Oversubscribed, prioritized'
        }
    coltitles={
        0:'Efficiency of row (fg) in presence of col (bg)',
        1:'Efficiency of col (bg) in presence of row (fg)',
        2:'Total efficiency'
        }

    c=1
    for x in xrange(0,3):
        for i in xrange(0,3):
            lo=[0.0,0.0,0.0][i]
            hi=[1.0,1.0,2.0][i]
            plt.subplot('33'+str(c))
            img=np.zeros(s)
            for j in xrange(len(jobnames)):
                for k in xrange(len(jobnames)):
                    img[k,j]=timings[x][jobnames[k]][jobnames[j]][i]
            plt.title(rowtitles[x]+'\n'+coltitles[i])
            plt.imshow(img,vmin=lo,vmax=hi,interpolation='none',cmap=plt.get_cmap('RdBu'))
            plt.colorbar()
            plt.xticks(np.arange(len(jobnames)),jobnames,rotation=90)
            plt.yticks(np.arange(len(jobnames)),jobnames)
            c=c+1
                
    plt.savefig('impact-'+experimentname+'.png')

def main():

    filenames=sorted(glob.glob('../results/output-*.txt'))

    for f in filenames:
        analyse(f)

if __name__ == '__main__':
    main()
