#!/usr/bin/env python

import glob
import matplotlib
matplotlib.use('Agg')    
import matplotlib.pyplot as plt
import numpy as np
import os
import scipy.stats

def isfloat(s):
    try:
        float(s)
        return True
    except ValueError:
        return False

def analyse(filename,i,figures):
    plt.subplot(str(figures)+"1"+str(i))

    experimentname=filename.split('/')[-1].replace('output-','').replace('.txt','')

    timings=[]

    for line in file(filename):
        if len(line)>0 and line[0]=='$':
            fields=map(lambda x: x.strip(),line.split(':'))
            for field in fields:
                if len(field)>=2 and field[-1]=='s' and isfloat(field[:-1]):
                    timings.append(float(field[:-1]))
                    break
            
    timings=np.float32(timings)

    lo=4.975
    hi=5.05

    plt.hist(timings,bins=np.arange(lo,hi,0.0025))
    plt.title(experimentname)
    plt.xlabel('Test duration (s)')
    plt.ylabel('Tests')
    plt.ylim([0,1000])
    plt.xlim([lo,hi])

    print "{:40}: {:10.3f} {:10.3f} {:10.3f} {:10.3f}".format(experimentname,timings.mean(),timings.std(),timings.min(),timings.max())

def main():

    filenames=sorted(glob.glob('../results/output-*.txt'))
    linuxfilenames=[x for x in filenames if 'wheezy' in x]
    windowsfilenames=[x for x in filenames if 'wheezy' not in x]
    
    plt.figure(figsize=(6,3*len(windowsfilenames)))
    print "{:40}: {:>10} {:>10} {:>10} {:>10}".format("Windows","Mean","SD","Min","Max")
    i=1
    for f in windowsfilenames:
        analyse(f,i,len(windowsfilenames))
        i=i+1
    plt.tight_layout()
    plt.savefig('timing-windows.png')

    plt.figure(figsize=(6,3*len(linuxfilenames)))
    print
    print "{:40}: {:>10} {:>10} {:>10} {:>10}".format("Linux","Mean","SD","Min","Max")
    i=1
    for f in linuxfilenames:
        analyse(f,i,len(linuxfilenames))
        i=i+1
    plt.tight_layout()
    plt.savefig('timing-linux.png')

if __name__ == '__main__':
    main()
