#include "force.h"

volatile bool dump;

void force(uint8_t v) {dump=(v!=0);}
void force(uint16_t v) {dump=(v!=0);}
void force(uint32_t v) {dump=(v!=0);}
void force(uint64_t v) {dump=(v!=0);}
