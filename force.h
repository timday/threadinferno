#ifndef __threadinferno_force_h__
#define __threadinferno_force_h__

#include <cstddef>
#include <cstdint>

extern void force(uint8_t);
extern void force(uint16_t);
extern void force(uint32_t);
extern void force(uint64_t);

#endif
