#include "job.h"

#include <algorithm>
#include <atomic>
#include <cstring>
#include <iostream>
#include <string>
#include <sstream>
#include <thread>
#include <utility>
#include <vector>

#include <tbb/blocked_range.h>
#include <tbb/blocked_range2d.h>
#include <tbb/blocked_range3d.h>
#include <tbb/parallel_for.h>
#include <tbb/parallel_reduce.h>
#include <tbb/pipeline.h>
#include <tbb/queuing_mutex.h>

#include "force.h"
#include "job_resources.h"
#include "random.h"
#include "workload.h"

job::job()
{}

job::~job()
{}

// Examine TBB 2D tiling pattern
std::string job::probe_tbb_blocked_range2d(size_t grainsize) {

  std::stringstream msg;
  tbb::queuing_mutex mutex;

  tbb::parallel_for(
    tbb::blocked_range2d<size_t,size_t>(0,1024,grainsize,0,1024,grainsize),

    [&mutex,&msg](const tbb::blocked_range2d<size_t,size_t>& r) {

      std::stringstream info;
      info 
        << r.rows().begin() << " " 
        << r.cols().begin() << " " 
        << r.rows().size() << " " 
        << r.cols().size() << " " 
        << std::this_thread::get_id() 
        << std::endl;
      {
        tbb::queuing_mutex::scoped_lock lock(mutex);
        msg << info.str();
      }

      // Load thread with a compute task, to be more realistic than nothing/sleep/yield etc.
      const aborter noabort;
      for (size_t row=r.rows().begin();row<r.rows().end();++row) {
        for (size_t col=r.cols().begin();col<r.cols().end();++col) {
          workload_base::iterate_hailstone(1+row*job::data_size_x+col,1024,noabort);
        }
      }
    }
  );
  return msg.str();
}

job_compute::job_compute()
{}

job_compute::~job_compute()
{}

std::string job_compute::units() const {
  return "GIter";
}

double job_compute::scale() const {
  return 1.0/double(1<<30);
}

uint64_t job_compute::run(const aborter& abort) {
  uint64_t t=0;
  while (!abort) {
    const std::pair<uint64_t,uint64_t> result=tbb::parallel_reduce(
      tbb::blocked_range<uint64_t>(1,1+(1<<20)),
      std::pair<uint64_t,uint64_t>(0,0),
      [&abort](const tbb::blocked_range<size_t>& r,const std::pair<uint64_t,uint64_t>& a) -> std::pair<uint64_t,uint64_t> {
        uint64_t b=0;
        uint64_t n=0;
        for (size_t i=r.begin();i<r.end() && !abort;++i) {
          const std::pair<uint64_t,size_t> r=workload_base::iterate_hailstone(i,(1<<20),abort);
          b+=r.first;
          n+=r.second;
        }
	return std::pair<uint64_t,size_t>(a.first+b,a.second+n);
      },
      [](const std::pair<uint64_t,uint64_t>& a,const std::pair<uint64_t,uint64_t>& b) -> std::pair<uint64_t,uint64_t> {
	return std::make_pair(a.first+b.first,a.second+b.second);
      }
    );
    force(result.first);
    t+=result.second;
  }
  return t;
}

job_trivial1d::job_trivial1d(size_t grainsize)
:_grainsize(grainsize)
{}

job_trivial1d::~job_trivial1d()
{}

std::string job_trivial1d::units() const {
  return "GItems";
}

double job_trivial1d::scale() const {
  return 1.0/double(1<<30);
}

uint64_t job_trivial1d::run(const aborter& abort) {
  uint64_t t=0;
  while (!abort) {
    const size_t result=tbb::parallel_reduce(
      tbb::blocked_range<size_t>(
        0,data_size,_grainsize
      ),
      size_t(0),
      [&abort](const tbb::blocked_range<size_t>& r,size_t a) -> size_t {
        return a+workload_base::spin(r.size(),abort);
      },
      [](size_t a,size_t b) -> size_t {
	return a+b;
      }
    );
    if (!abort && result!=data_size) {
      std::cerr << "Unexpected trivial1d" << std::endl;
    }
    t+=result;
  }
  return t;
}

job_trivial2d::job_trivial2d(size_t grainsize)
:_grainsize(grainsize)
{}

job_trivial2d::~job_trivial2d()
{}

std::string job_trivial2d::units() const {
  return "GItems";
}

double job_trivial2d::scale() const {
  return 1.0/double(1<<30);
}

uint64_t job_trivial2d::run(const aborter& abort) {
  uint64_t t=0;
  while (!abort) {
    const size_t result=tbb::parallel_reduce(
      tbb::blocked_range2d<size_t,size_t>(
        0,data_size_y*data_size_z_sqrt,_grainsize,
        0,data_size_x*data_size_z_sqrt,_grainsize
      ),
      size_t(0),
      [&abort](const tbb::blocked_range2d<size_t,size_t>& r,size_t a) -> size_t {
        return a+workload_base::spin(r.rows().size()*r.cols().size(),abort);
      },
      [](size_t a,size_t b) -> size_t {
	return a+b;
      }
    );
    if (!abort && result!=data_size) {
      std::cerr << "Unexpected trivial2d result" << std::endl;
    }
    t+=result;
  }
  return t;
}

job_trivial3d::job_trivial3d(size_t grainsize)
:_grainsize(grainsize)
{}

job_trivial3d::~job_trivial3d()
{}

std::string job_trivial3d::units() const {
  return "GItems";
}

double job_trivial3d::scale() const {
  return 1.0/double(1<<30);
}

uint64_t job_trivial3d::run(const aborter& abort) {
  uint64_t t=0;
  while (!abort) {
    const size_t result=tbb::parallel_reduce(
      tbb::blocked_range3d<size_t,size_t,size_t>(
        0,data_size_z,_grainsize,
        0,data_size_y,_grainsize,
        0,data_size_x,_grainsize
      ),
      size_t(0),
      [&abort](const tbb::blocked_range3d<size_t,size_t,size_t>& r,size_t a) -> size_t {
        return a+workload_base::spin(r.pages().size()*r.rows().size()*r.cols().size(),abort);
      },
      [](size_t a,size_t b) -> size_t {
	return a+b;
      }
    );
    if (!abort && result!=data_size) {
      std::cerr << "Unexpected trivial2d result" << std::endl;
    }
    t+=result;
  }
  return t;
}

job_memsum::job_memsum()
{}

job_memsum::~job_memsum()
{}

std::string job_memsum::units() const {
  return "GByte";
}

double job_memsum::scale() const {
  return sizeof(data_t)/double(1<<30);
}

uint64_t job_memsum::run(const aborter& abort) {
  uint64_t t=0;
  while (!abort) {
    const std::pair<data_t,size_t> result=tbb::parallel_reduce(
      tbb::blocked_range<size_t>(0,job_resources::data().size()),
      std::pair<data_t,size_t>(0,0),
      [&abort](const tbb::blocked_range<size_t>& r,const std::pair<data_t,size_t>& a) -> std::pair<data_t,size_t> {
	const std::pair<data_t,size_t> b=workload<data_t>::memsum(&job_resources::data()[r.begin()],r.size(),abort);
	return std::make_pair(a.first+b.first,a.second+b.second);
      },
      [](const std::pair<data_t,size_t>& a,const std::pair<data_t,size_t>& b) -> std::pair<data_t,size_t> {
	return std::make_pair(a.first+b.first,a.second+b.second);
      }
    );
    force(result.first);
    if (!abort && result.first!=job_resources::data_sum()[0]) {
      std::cerr << "Unexpected memsum result" << std::endl;
    }
    t+=result.second;
  }
  return t;
}

namespace {

  std::array<size_t,3> permuted_sizes(int x,int y,int z) {
    std::array<size_t,3> s;
    s[x]=job::data_size_x;
    s[y]=job::data_size_y;
    s[z]=job::data_size_z;
    return s;
  }

  std::array<size_t,3> permuted_strides(int x,int y,int z) {
    std::array<size_t,3> s;
    s[x]=1;
    s[y]=job::data_size_x;
    s[z]=job::data_size_y*job::data_size_x;
    return s;
  }
}

job_memsum_strided_base::job_memsum_strided_base(int x,int y,int z,bool fastx)
  :_size(permuted_sizes(x,y,z))
  ,_stride(permuted_strides(x,y,z))
  ,_fastx(fastx)
{}
 
job_memsum_strided_base::~job_memsum_strided_base()
{}

std::string job_memsum_strided_base::units() const {
  return "GByte";
}

double job_memsum_strided_base::scale() const {
  return sizeof(data_t)/double(1<<30);
}

job_memsum_strided::job_memsum_strided(int x,int y,int z,bool fastx)
  :job_memsum_strided_base(x,y,z,fastx)
{}
 
job_memsum_strided::~job_memsum_strided()
{}

uint64_t job_memsum_strided::run(const aborter& abort) {
  uint64_t t=0;
  while (!abort) {
    const std::pair<data_t,size_t> result=tbb::parallel_reduce(
      tbb::blocked_range2d<size_t,size_t>(0,_size[0],0,_size[1]),
      std::pair<data_t,size_t>(0,0),
      [&abort,this](const tbb::blocked_range2d<size_t,size_t>& r,const std::pair<data_t,size_t>& a) -> std::pair<data_t,size_t> {
        std::pair<data_t,size_t> t(a);
        const data_t* jptr=&job_resources::data()[r.rows().begin()*_stride[0]];
        for (size_t j=r.rows().begin();j<r.rows().end() && !abort;++j,jptr+=_stride[0]) {
          const data_t* iptr=jptr+r.cols().begin()*_stride[1];
          for (size_t i=r.cols().begin();i<r.cols().end() && !abort;++i,iptr+=_stride[1]) {
            const std::pair<data_t,size_t> b=(
              _fastx && _stride[2]==1 
              ? workload<data_t>::memsum(iptr,_size[2],abort)
              : workload<data_t>::memsum_strided(iptr,_size[2],_stride[2],abort)
            );
            t.first+=b.first;
            t.second+=b.second;
          }
        }
        return t;
      },
      [](const std::pair<data_t,size_t>& a,const std::pair<data_t,size_t>& b) -> std::pair<data_t,size_t> {
	return std::make_pair(a.first+b.first,a.second+b.second);
      }
    );
    force(result.first);
    if (!abort && result.first!=job_resources::data_sum()[0]) {
      std::cerr << "Unexpected memsum result in job_memsum_strided" << std::endl;
    }
    t+=result.second;    
  }
  return t;
}

job_memsamp_strided::job_memsamp_strided(int x,int y,int z,size_t d)
  :job_memsum_strided_base(x,y,z,false)
  ,_d(d)
{}
 
job_memsamp_strided::~job_memsamp_strided()
{}

std::string job_memsamp_strided::units() const {
  return "MSamp";
}

double job_memsamp_strided::scale() const {
  return 1.0/double(1<<20);
}

uint64_t job_memsamp_strided::run(const aborter& abort) {
  uint64_t t=0;
  while (!abort) {
    const std::pair<data_t,size_t> result=tbb::parallel_reduce(
      tbb::blocked_range2d<size_t,size_t>(0,_size[0]-_d,0,_size[1]-_d),
      std::pair<data_t,size_t>(0,0),
      [&abort,this](const tbb::blocked_range2d<size_t,size_t>& r,const std::pair<data_t,size_t>& a) -> std::pair<data_t,size_t> {
        std::pair<data_t,size_t> t(a);
        const data_t* jptr=&job_resources::data()[r.rows().begin()*_stride[0]];
        for (size_t j=r.rows().begin();j<r.rows().end() && !abort;++j,jptr+=_stride[0]) {
          const data_t* iptr=jptr+r.cols().begin()*_stride[1];
          for (size_t i=r.cols().begin();i<r.cols().end() && !abort;++i,iptr+=_stride[1]) {
            const std::pair<data_t,size_t> b=workload<data_t>::memsamp_strided(iptr,_size[2]-_d,_stride[2],_d,_stride[0],_stride[1],abort);
            t.first+=b.first;
            t.second+=b.second;
          }
        }
        return t;
      },
      [](const std::pair<data_t,size_t>& a,const std::pair<data_t,size_t>& b) -> std::pair<data_t,size_t> {
	return std::make_pair(a.first+b.first,a.second+b.second);
      }
    );
    force(result.first);
    if (!abort && result.first!=job_resources::data_sum()[_d]) {
      std::cerr << "Unexpected memsum result in job_memsamp_strided" << std::endl;
    }
    t+=result.second;    
  }
  return t;
}

job_memsum_strided_tight::job_memsum_strided_tight(int x,int y,int z,bool fastx)
  :job_memsum_strided_base(x,y,z,fastx)
{}
 
job_memsum_strided_tight::~job_memsum_strided_tight()
{}

uint64_t job_memsum_strided_tight::run(const aborter& abort) {
  uint64_t t=0;
  std::atomic<size_t> index;
  while (!abort) {
    index=0;
    const std::pair<data_t,size_t> result=tbb::parallel_reduce(
      tbb::blocked_range<size_t>(0,_size[0]*_size[1]),
      std::pair<data_t,size_t>(0,0),
      [&abort,&index,this](const tbb::blocked_range<size_t>& r,const std::pair<data_t,size_t>& a) -> std::pair<data_t,size_t> {
        std::pair<data_t,size_t> t(a);
        for (size_t i=0;i<r.size() && !abort;++i) {
          const size_t idx=index++; // Atomic
          const data_t*const ptr=&job_resources::data()[0]+_stride[0]*(idx/_size[1])+_stride[1]*(idx%_size[1]);
          const std::pair<data_t,size_t> b=(
            _fastx && _stride[2]==1 
            ? workload<data_t>::memsum(ptr,_size[2],abort)
            : workload<data_t>::memsum_strided(ptr,_size[2],_stride[2],abort)
          );
          t.first+=b.first;
          t.second+=b.second;
        }
        return t;
      },
      [](const std::pair<data_t,size_t>& a,const std::pair<data_t,size_t>& b) -> std::pair<data_t,size_t> {
	return std::make_pair(a.first+b.first,a.second+b.second);
      }
    );
    force(result.first);
    if (!abort && result.first!=job_resources::data_sum()[0]) {
      std::cerr << "Unexpected memsum result in job_memsum_strided_tight" << std::endl;
    }
    t+=result.second;    
  }
  return t;
}

job_memsamp_strided_tight::job_memsamp_strided_tight(int x,int y,int z,size_t d)
  :job_memsum_strided_base(x,y,z,false)
  ,_d(d)
{}
 
job_memsamp_strided_tight::~job_memsamp_strided_tight()
{}

std::string job_memsamp_strided_tight::units() const {
  return "MSamp";
}

double job_memsamp_strided_tight::scale() const {
  return 1.0/double(1<<20);
}

uint64_t job_memsamp_strided_tight::run(const aborter& abort) {
  uint64_t t=0;
  std::atomic<size_t> index;
  while (!abort) {
    index=0;
    const std::pair<data_t,size_t> result=tbb::parallel_reduce(
      tbb::blocked_range<size_t>(0,(_size[0]-_d)*(_size[1]-_d)),
      std::pair<data_t,size_t>(0,0),
      [&abort,&index,this](const tbb::blocked_range<size_t>& r,const std::pair<data_t,size_t>& a) -> std::pair<data_t,size_t> {
        std::pair<data_t,size_t> t(a);
        for (size_t i=0;i<r.size() && !abort;++i) {
          const size_t idx=index++; // Atomic
          const size_t row=idx/(_size[1]-_d);
          const size_t col=idx%(_size[1]-_d);
          const data_t*const ptr=&job_resources::data()[0]+_stride[0]*row+_stride[1]*col;
          const std::pair<data_t,size_t> b=workload<data_t>::memsamp_strided(ptr,_size[2]-_d,_stride[2],_d,_stride[0],_stride[1],abort);
          t.first+=b.first;
          t.second+=b.second;
        }
        return t;
      },
      [](const std::pair<data_t,size_t>& a,const std::pair<data_t,size_t>& b) -> std::pair<data_t,size_t> {
	return std::make_pair(a.first+b.first,a.second+b.second);
      }
    );
    force(result.first);
    if (!abort && result.first!=job_resources::data_sum()[_d]) {
      std::cerr << "Unexpected memsum result in job_memsamp_strided" << std::endl;
    }
    t+=result.second;    
  }
  return t;
}

job_memsum_3d::job_memsum_3d(bool fastx)
:_fastx(fastx)
{}
 
job_memsum_3d::~job_memsum_3d()
{}

std::string job_memsum_3d::units() const {
  return "GByte";
}

double job_memsum_3d::scale() const {
  return sizeof(data_t)/double(1<<30);
}

uint64_t job_memsum_3d::run(const aborter& abort) {
  uint64_t t=0;
  while (!abort) {
    const std::pair<data_t,size_t> result=tbb::parallel_reduce(
      tbb::blocked_range3d<size_t,size_t,size_t>(0,data_size_z,0,data_size_y,0,data_size_z),
      std::pair<data_t,size_t>(0,0),
      [&abort,this](const tbb::blocked_range3d<size_t,size_t,size_t>& r,const std::pair<data_t,size_t>& a) -> std::pair<data_t,size_t> {
        std::pair<data_t,size_t> t(a);
        for (size_t z=r.pages().begin();z<r.pages().end() && !abort;++z) {
          for (size_t y=r.rows().begin();y<r.rows().end() && !abort;++y) {
            const data_t*const p=&job_resources::data()[z*(data_size_x*data_size_y)+y*data_size_x+r.cols().begin()];
            const std::pair<data_t,size_t> b=(
              _fastx
              ? workload<data_t>::memsum(p,r.cols().size(),abort)
              : workload<data_t>::memsum_strided(p,r.cols().size(),1,abort)
            );
            t.first+=b.first;
            t.second+=b.second;
          }
        }
        return t;
      },
      [](const std::pair<data_t,size_t>& a,const std::pair<data_t,size_t>& b) -> std::pair<data_t,size_t> {
	return std::make_pair(a.first+b.first,a.second+b.second);
      }
    );
    force(result.first);
    if (!abort && result.first!=job_resources::data_sum()[0]) {
      std::cerr << "Unexpected memsum result" << std::endl;
    }
    t+=result.second;    
  }
  return t;
}

job_memcpy::job_memcpy()
{
  _dst.resize(data_size);
}

job_memcpy::~job_memcpy()
{}

std::string job_memcpy::units() const {
  return "GByte";
}

double job_memcpy::scale() const {
  return sizeof(data_t)/double(1<<30);
}

uint64_t job_memcpy::run(const aborter& abort) {
  uint64_t t=0;
  while (!abort) {
    const size_t r=tbb::parallel_reduce(
      tbb::blocked_range<size_t>(0,data_size),
      size_t(0),
      [&abort,this](const tbb::blocked_range<size_t>& r,size_t a) -> size_t {
        if (abort) {
          return a;
        } else {
          memcpy(&_dst[r.begin()],&job_resources::data()[r.begin()],sizeof(data_t)*r.size());
          return a+r.size();
        }
      },
      [](size_t a,size_t b) -> size_t {
	return a+b;
      }
    );
    t+=r;    
  }
  return t;
}

job_memset::job_memset()
{
  _dst.resize(data_size);
}

job_memset::~job_memset()
{}

std::string job_memset::units() const {
  return "GByte";
}

double job_memset::scale() const {
  return sizeof(data_t)/double(1<<30);
}

uint64_t job_memset::run(const aborter& abort) {
  uint64_t t=0;
  while (!abort) {
    const size_t r=tbb::parallel_reduce(
      tbb::blocked_range<size_t>(0,data_size),
      size_t(0),
      [&abort,this](const tbb::blocked_range<size_t>& r,size_t a) -> size_t {
        if (abort) {
          return a;
        } else {
          memset(&_dst[r.begin()],0xff,sizeof(data_t)*r.size());
          return a+r.size();
        }
      },
      [](size_t a,size_t b) -> size_t {
	return a+b;
      }
    );
    t+=r;    
  }
  return t;
}

job_tlbthrash_read::job_tlbthrash_read()
{
  const size_t pages=data_size_bytes/page_size_bytes;
  for (size_t i=0;i<pages;++i) {
    _offset.push_back((i*page_size_bytes)/sizeof(data_t));
  }
  std::random_shuffle(_offset.begin(),_offset.end());
}

job_tlbthrash_read::~job_tlbthrash_read()
{}

std::string job_tlbthrash_read::units() const {
  return "M4kPage";
}

double job_tlbthrash_read::scale() const {
  return 1.0/double(1<<20);
}

uint64_t job_tlbthrash_read::run(const aborter& abort) {
  uint64_t t=0;
  size_t d=0;
  while (!abort) {
    const std::pair<data_t,size_t> result=tbb::parallel_reduce(
      tbb::blocked_range<size_t>(0,_offset.size()),
      std::pair<data_t,size_t>(0,0),
      [&abort,d,this](const tbb::blocked_range<size_t>& r,const std::pair<data_t,size_t>& a) -> std::pair<data_t,size_t> {
        data_t t=0;
        size_t n=0;
        for (size_t i=r.begin();i<r.end() && !abort;++i) {
          t+=job_resources::data()[_offset[i]+d];
          ++n;
        }
	return std::make_pair(a.first+t,a.second+n);
      },
      [](const std::pair<data_t,size_t>& a,const std::pair<data_t,size_t>& b) -> std::pair<data_t,size_t> {
	return std::make_pair(a.first+b.first,a.second+b.second);
      }
    );
    force(result.first);
    t+=result.second;
    d=random_int(0,page_size_bytes/sizeof(data_t)-1);
  }
  return t;
}

job_tlbthrash_copy::job_tlbthrash_copy()
  :_dst(data_size,0)
{
  const size_t pages=data_size_bytes/page_size_bytes;
  for (size_t i=0;i<pages;++i) {
    _offset_src.push_back((i*page_size_bytes)/sizeof(data_t));
    _offset_dst.push_back((i*page_size_bytes)/sizeof(data_t));
  }
  std::random_shuffle(_offset_src.begin(),_offset_src.end());
  std::random_shuffle(_offset_dst.begin(),_offset_dst.end());
}

job_tlbthrash_copy::~job_tlbthrash_copy()
{}

std::string job_tlbthrash_copy::units() const {
  return "M4kPage";
}

double job_tlbthrash_copy::scale() const {
  return 1.0/double(1<<20);
}

uint64_t job_tlbthrash_copy::run(const aborter& abort) {
  uint64_t t=0;
  size_t d_dst=0;
  size_t d_src=0;
  while (!abort) {
    const size_t result=tbb::parallel_reduce(
      tbb::blocked_range<size_t>(0,_offset_dst.size()),
      size_t(0),
      [&abort,d_dst,d_src,this](const tbb::blocked_range<size_t>& r,size_t a) -> size_t {
        data_t t=0;
        for (size_t i=r.begin();i<r.end() && !abort;++i) {
          _dst[_offset_dst[i]+d_dst]=job_resources::data()[_offset_src[i]+d_src];
          ++a;
        }
	return a;
      },
      [](size_t a,size_t b) -> size_t {
	return a+b;
      }
    );
    t+=result;
    d_dst=random_int(0,page_size_bytes/sizeof(data_t)-1);
    d_src=random_int(0,page_size_bytes/sizeof(data_t)-1);
  }
  return t;
}

job_tlbthrash_write::job_tlbthrash_write()
  :_dst(data_size,0)
{
  const size_t pages=data_size_bytes/page_size_bytes;
  for (size_t i=0;i<pages;++i) {
    _offset.push_back((i*page_size_bytes)/sizeof(data_t));
  }
  std::random_shuffle(_offset.begin(),_offset.end());
}

job_tlbthrash_write::~job_tlbthrash_write()
{}

std::string job_tlbthrash_write::units() const {
  return "M4kPage";
}

double job_tlbthrash_write::scale() const {
  return 1.0/double(1<<20);
}

uint64_t job_tlbthrash_write::run(const aborter& abort) {
  uint64_t t=0;
  size_t d=0;
  while (!abort) {
    const size_t result=tbb::parallel_reduce(
      tbb::blocked_range<size_t>(0,_offset.size()),
      size_t(0),
      [&abort,d,this](const tbb::blocked_range<size_t>& r,size_t a) -> size_t {
        data_t t=0;
        for (size_t i=r.begin();i<r.end() && !abort;++i) {
          _dst[_offset[i]+d]=0xff;
          ++a;
        }
	return a;
      },
      [](size_t a,size_t b) -> size_t {
	return a+b;
      }
    );
    t+=result;
    d=random_int(0,page_size_bytes/sizeof(data_t)-1);
  }
  return t;
}

job_smallmem::job_smallmem(size_t size_kb)
  :_buffer((size_kb<<10)/sizeof(uint64_t),0)
{}

job_smallmem::~job_smallmem()
{}

std::string job_smallmem::units() const {
  return "Giga-64bit-increments";
}

double job_smallmem::scale() const {
  return 1.0/double(1<<30);
}

uint64_t job_smallmem::run(const aborter& abort) {
  uint64_t t=0;
  while (!abort) {
    const size_t result=tbb::parallel_reduce(
      tbb::blocked_range<size_t>(0,_buffer.size()),
      size_t(0),
      [&abort,this](const tbb::blocked_range<size_t>& r,size_t a) -> size_t {
        return a+workload_base::memmod(&_buffer[r.begin()],&_buffer[r.end()],abort);
      },
      [](size_t a,size_t b) -> size_t {
	return a+b;
      }
    );
    t+=result;
  }
  return t;
}

job_alloc::job_alloc()
{
  _dst.resize(data_size_y*data_size_z);
  for (size_t i=0;i<_dst.size();++i) {
    std::vector<uint8_t> replacement(random_int(1,127),0xff);
    _dst[i].swap(replacement);
  }
}

job_alloc::~job_alloc()
{}

std::string job_alloc::units() const {
  return "Mega-allocate-and-free";
}

double job_alloc::scale() const {
  return 1.0/double(1<<20);
}

uint64_t job_alloc::run(const aborter& abort) {
  uint64_t t=0;
  while (!abort) {
    const size_t r=tbb::parallel_reduce(
      tbb::blocked_range<size_t>(0,_dst.size()),
      size_t(0),
      [&abort,this](const tbb::blocked_range<size_t>& r,size_t a) -> size_t {
        for (size_t i=r.begin();i<r.end() && !abort;++i) {
          std::vector<uint8_t> replacement(random_int(1,127),i);
          _dst[i].swap(replacement);
          ++a;
        }
        return a;
      },
      [](size_t a,size_t b) -> size_t {
	return a+b;
      }
    );
    t+=r;
  }
  return t;
}

job_file_read::job_file_read()
{}

job_file_read::~job_file_read()
{}

std::string job_file_read::units() const {
  return "GByte";
}

double job_file_read::scale() const {
  return 1.0/double(1<<30);
}

uint64_t job_file_read::run(const aborter& abort) {
  uint64_t t=0;
  while (!abort) {
    const std::pair<uint8_t,size_t> result=tbb::parallel_reduce(
      tbb::blocked_range<size_t>(0,job_resources::files().size()),
      std::pair<uint8_t,size_t>(0,0),
      [&abort](const tbb::blocked_range<size_t>& r,const std::pair<uint8_t,size_t>& a) -> std::pair<uint8_t,size_t> {
        std::pair<uint8_t,size_t> t(a);
        for (size_t i=r.begin();i<r.end();++i) {
          const std::pair<uint8_t,size_t> b=workload_base::filesum(job_resources::files()[i].c_str(),job::file_size,abort);
          t.first+=b.first;
          t.second+=b.second;
        }
        return t;
      },
      [](const std::pair<uint8_t,size_t>& a,const std::pair<uint8_t,size_t>& b) -> std::pair<uint8_t,size_t> {
	return std::make_pair(a.first+b.first,a.second+b.second);
      }
    );
    force(result.first);
    t+=result.second;
  }
  return t;
}
