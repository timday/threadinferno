#ifndef __threadinferno_job_h__
#define __threadinferno_job_h__

#include <array>
#include <cstdint>
#include <cstddef>
#include <memory>
#include <string>
#include <vector>

#include "aborter.h"

class job {
public:
  job();
  virtual ~job();

  virtual std::string units() const = 0;
  virtual double scale() const = 0;
  virtual uint64_t run(const aborter&) = 0;

  typedef uint16_t data_t;

  static const size_t data_size_x=1024+2;
  static const size_t data_size_y=1024;
  static const size_t data_size_z=1024;
  static const size_t data_size_z_sqrt=32;
  static const size_t data_size=data_size_x*data_size_y*data_size_z;
  
  static const size_t data_size_bytes=data_size*sizeof(data_t);

  static const size_t page_size_bytes=4096;

  static const size_t file_size=(4<<20);
  static const size_t number_of_files=256;

  static std::string probe_tbb_blocked_range2d(size_t);
};

class job_trivial1d : public job {
public:
  job_trivial1d(size_t grainsize);
  virtual ~job_trivial1d();
  
  virtual std::string units() const;
  virtual double scale() const;
  virtual uint64_t run(const aborter&) override;
private:
  const size_t _grainsize;
};

class job_trivial2d : public job {
public:
  job_trivial2d(size_t grainsize);
  virtual ~job_trivial2d();
  
  virtual std::string units() const;
  virtual double scale() const;
  virtual uint64_t run(const aborter&) override;
private:
  const size_t _grainsize;
};

class job_trivial3d : public job {
public:
  job_trivial3d(size_t grainsize);
  virtual ~job_trivial3d();
  
  virtual std::string units() const;
  virtual double scale() const;
  virtual uint64_t run(const aborter&) override;
private:
  const size_t _grainsize;
};

class job_compute : public job {
public:
  job_compute();
  virtual ~job_compute();
  
  virtual std::string units() const;
  virtual double scale() const;
  virtual uint64_t run(const aborter&) override;
};

class job_memsum : public job {
public:
  job_memsum();
  ~job_memsum() override;
  
  std::string units() const override;
  double scale() const override;
  uint64_t run(const aborter&) override;
};

class job_memsum_3d : public job {
public:
  job_memsum_3d(bool fastx);
  ~job_memsum_3d() override;
  
  std::string units() const override;
  double scale() const override;
  uint64_t run(const aborter&) override;
private:
  const bool _fastx;
};

class job_memsum_strided_base : public job {
public:
  job_memsum_strided_base(int x,int y,int z,bool fastx);
  ~job_memsum_strided_base() override;
  
  std::string units() const override;
  double scale() const override;
protected:
  const std::array<size_t,3> _size;
  const std::array<size_t,3> _stride;
  const bool _fastx;
};

class job_memsum_strided : public job_memsum_strided_base {
public:
  job_memsum_strided(int x,int y,int z,bool fastx);
  ~job_memsum_strided() override;
  
  uint64_t run(const aborter&) override;
};

class job_memsamp_strided : public job_memsum_strided_base {
public:
  job_memsamp_strided(int x,int y,int z,size_t d);
  ~job_memsamp_strided() override;
  
  std::string units() const override;
  double scale() const override;
  uint64_t run(const aborter&) override;
private:
  const size_t _d;
};

class job_memsum_strided_tight : public job_memsum_strided_base {
public:
  job_memsum_strided_tight(int x,int y,int z,bool fastx);
  ~job_memsum_strided_tight() override;
  
  uint64_t run(const aborter&) override;
};

class job_memsamp_strided_tight : public job_memsum_strided_base {
public:
  job_memsamp_strided_tight(int x,int y,int z,size_t d);
  ~job_memsamp_strided_tight() override;
  
  std::string units() const override;
  double scale() const override;
  uint64_t run(const aborter&) override;
private:
  const size_t _d;
};

class job_memcpy : public job {
public:
  job_memcpy();
  ~job_memcpy() override;
  
  std::string units() const override;
  double scale() const override;
  uint64_t run(const aborter&) override;

private:
  std::vector<data_t> _dst;
};

class job_memset : public job {
public:
  job_memset();
  ~job_memset() override;
  
  std::string units() const override;
  double scale() const override;
  uint64_t run(const aborter&) override;

private:
  std::vector<data_t> _dst;
};

class job_tlbthrash_read : public job {
public:
  job_tlbthrash_read();
  ~job_tlbthrash_read() override;
  
  std::string units() const override;
  double scale() const override;
  uint64_t run(const aborter&) override;
private:
  std::vector<size_t> _offset;
};

class job_tlbthrash_copy : public job {
public:
  job_tlbthrash_copy();
  ~job_tlbthrash_copy() override;
  
  std::string units() const override;
  double scale() const override;
  uint64_t run(const aborter&) override;
private:
  std::vector<data_t> _dst;
  std::vector<size_t> _offset_src;
  std::vector<size_t> _offset_dst;
};

class job_tlbthrash_write : public job {
public:
  job_tlbthrash_write();
  ~job_tlbthrash_write() override;
  
  std::string units() const override;
  double scale() const override;
  uint64_t run(const aborter&) override;
private:
  std::vector<data_t> _dst;
  std::vector<size_t> _offset;
};

class job_smallmem : public job {
public:
  job_smallmem(size_t size_kb);
  ~job_smallmem() override;
  
  std::string units() const override;
  double scale() const override;
  uint64_t run(const aborter&) override;
private:
  std::vector<uint64_t> _buffer;
};

class job_alloc : public job {
public:
  job_alloc();
  ~job_alloc() override;
  
  std::string units() const override;
  double scale() const override;
  uint64_t run(const aborter&) override;

private:
  std::vector<std::vector<uint8_t> > _dst;
};

class job_file_read : public job {
public: 
  job_file_read();
  ~job_file_read() override;
  
  std::string units() const override;
  double scale() const override;
  uint64_t run(const aborter&) override;
};

#endif
