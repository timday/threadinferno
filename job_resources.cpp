#include "job_resources.h"

#include <cstdio>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <memory>
#include <numeric>
#include <sstream>
#include <tbb/blocked_range3d.h>
#include <tbb/parallel_reduce.h>

#include "force.h"
#include "workload.h"

std::vector<job::data_t> job_resources::_data;
std::vector<job::data_t> job_resources::_data_sum;
std::vector<std::string> job_resources::_files;

#ifdef WIN32
const char*const path_separator="\\";
#else
const char*const path_separator="/";
#endif

job_resources::job_resources(const std::string& path,size_t samp_range) {

  std::cout << "Initializing data..." << std::endl;
  
  _data.resize(job::data_size,0);
  for (size_t i=0;i<_data.size();++i) {
    _data[i]=job::data_t(i);
  }
  _data[0]=23;
  _data[_data.size()/3]=job::data_t(-1);
  _data[_data.size()/2]=77;
  _data[_data.size()-1]=255;

  {
    for (size_t d=0;d<=samp_range;++d) {
      const job::data_t t=tbb::parallel_reduce(
        tbb::blocked_range3d<size_t>(
          0,job::data_size_z-d,
          0,job::data_size_y-d,
          0,job::data_size_x-d
        ),
        job::data_t(0),
        [d](const tbb::blocked_range3d<size_t>& r,job::data_t t) -> job::data_t {
          for (size_t z=r.pages().begin();z<r.pages().end();++z) {
            for (size_t y=r.rows().begin();y<r.rows().end();++y) {
              for (size_t x=r.cols().begin();x<r.cols().end();++x) {
                for (size_t dz=0;dz<=d;++dz) {
                  for (size_t dy=0;dy<=d;++dy) {
                    for (size_t dx=0;dx<=d;++dx) {
                      t+=_data[
                        (z+dz)*job::data_size_x*job::data_size_y
                        +
                        (y+dy)*job::data_size_x
                        +
                        (x+dx)
                      ];
                    }
                  }
                }
              }
            }
          }
          return t;
        },
        [](job::data_t a,job::data_t b)->job::data_t {
          return a+b;
        }
      );
      _data_sum.push_back(t);
    }
  }

  std::cout << "...initialized data" << std::endl;

  for (size_t i=0;i<job::number_of_files;++i) {
    std::vector<uint8_t> buffer(job::file_size,1+i);
    std::ostringstream filename;
    filename << path << path_separator << "threadinferno" << std::setfill('0') << std::setw(4) << i << ".tmp";
    std::ofstream out(filename.str().c_str(),std::ofstream::out|std::ofstream::trunc|std::ofstream::binary);
    out.write(reinterpret_cast<const char*>(&buffer[0]),buffer.size());
    out.close();
    if (!out) {
      std::cerr << "Error creating/writing " << filename.str() << std::endl;
    } else {
      _files.push_back(filename.str());
    }
  }

  std::cout << "Initialized files" << std::endl;

  const aborter noabort;
  for (std::vector<std::string>::const_iterator it=_files.begin();it<_files.end();++it) {
    force(workload_base::filesum((*it).c_str(),job::data_size_x*job::data_size_y,noabort).first);
  }

  std::cout << "Read back files" << std::endl;
}

job_resources::~job_resources() {

  {
    std::vector<job::data_t> empty;
    _data.swap(empty);
  }

  for (std::vector<std::string>::const_iterator it=_files.begin();it<_files.end();++it) {
    if (std::remove((*it).c_str())!=0) {
      std::cerr << "Error removing " << (*it)<< std::endl;
    }
  }
}
