#ifndef __threadinferno_job_resources_h__
#define __threadinferno_job_resources_h__

#include <string>
#include <utility>

#include "job.h"

class job_resources {

public:

  job_resources(const std::string&,size_t samp_range);
  ~job_resources();

  static const std::vector<job::data_t>& data() {
    return _data;
  }

  static const std::vector<job::data_t>& data_sum() {
    return _data_sum;
  }
  
  static const std::vector<std::string> files() {
    return _files;
  }

private:

  static std::vector<job::data_t> _data;
  static std::vector<job::data_t> _data_sum;
  static std::vector<std::string> _files;
};

#endif
