ARCH=$(shell arch)

WORKSPACE ?= /tmp

SOURCES=aborter.cpp force.cpp job.cpp job_resources.cpp random.cpp tbb_control.cpp tbb_prioritizer.cpp workload.cpp threadinferno.cpp
HEADERS=aborter.h   force.h   job.h   job_resources.h   random.h   tbb_control.h   tbb_prioritizer.h   workload.h   

CPPFLAGS=-std=c++11 -march=native -O3 -DNDEBUG -msse2 -mno-avx -g
DEFINES=-D__TBB_EXTRA_DEBUG -DTBB_PREVIEW_TASK_ARENA
LIBRARIES=-ltbb -lboost_program_options -lpthread

all:		aborter.s workload.s threadinferno
		sudo time ./threadinferno $(WORKSPACE)

threadinferno: 	makefile $(SOURCES) $(HEADERS)
		g++ $(SOURCES) $(CPPFLAGS) -o threadinferno $(INCLUDES) $(DEFINES) $(LIBRARIES)

aborter.s:	makefile aborter.h aborter.cpp
		g++ -S aborter.cpp $(CPPFLAGS) $(INCLUDES) $(DEFINES)

workload.s:	makefile workload.h workload.cpp
		g++ -S workload.cpp $(CPPFLAGS) $(INCLUDES) $(DEFINES)
