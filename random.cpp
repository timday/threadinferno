#include "random.h"

#include <random>
#include <ctime>
#include <thread>

#if defined (_MSC_VER)  // Visual studio
    #define thread_local __declspec( thread )
#elif defined (__GNUC__) // GCC
    #define thread_local __thread
#endif

// From http://stackoverflow.com/questions/21237905/how-do-i-generate-thread-safe-uniform-random-numbers

/* Thread-safe function that returns a random number between min and max (inclusive).
   This function takes ~142% the time that calling rand() would take. For this extra
   cost you get a better uniform distribution and thread-safety. */
int random_int(int minimum,int maximum) {
  static thread_local std::mt19937* generator = nullptr;
  if (!generator) {
    generator=new std::mt19937(clock()+std::hash<std::thread::id>()(std::this_thread::get_id()));
  }
  std::uniform_int_distribution<int> distribution(minimum, maximum);
  return distribution(*generator);
}
