Number of cores is 8
sizeof(size_t) is 8
sizeof(uint64_t) is 8
Base data size is (zyx) 1024 x 1024 x 1026 x 2 bytes
File I/O test files (256 4MByte files) will be created in /tmp
Initializing data...
...initialized data
Initialized files
Read back files
Testing blocked_range2d...
...wrote blocked_range2d-1.txt, blocked_range2d-32.txt next...
...tested blocked_range2d
> Single job, single-threaded, normal priority (scaling reference): 
$ trivial1d                     :    5.000s:    1.427 GItems/s                      
$ trivial2d                     :    5.000s:    1.427 GItems/s                      
$ trivial3d                     :    5.000s:    1.434 GItems/s                      
$ compute                       :    5.000s:    0.763 GIter/s                       
$ memsum                        :    5.000s:   12.760 GByte/s                       
$ memcpy                        :    5.008s:    3.426 GByte/s                       
$ memset                        :    5.002s:    5.340 GByte/s                       
$ tlbthrash_read                :    5.000s:   28.998 M4kPage/s                     
$ tlbthrash_copy                :    5.000s:   13.093 M4kPage/s                     
$ tlbthrash_write               :    5.000s:   29.845 M4kPage/s                     
$ memsum_zyx_fastx              :    5.000s:   11.875 GByte/s                       
$ memsum_yzx_fastx              :    5.000s:    8.646 GByte/s                       
$ memsum_zyx                    :    5.000s:    2.729 GByte/s                       
$ memsum_yzx                    :    5.000s:    2.553 GByte/s                       
$ memsum_zxy                    :    5.000s:    0.888 GByte/s                       
$ memsum_xzy                    :    5.000s:    0.239 GByte/s                       
$ memsum_yxz                    :    5.000s:    0.082 GByte/s                       
$ memsum_xyz                    :    5.000s:    0.077 GByte/s                       
$ memsum_tight_zyx_fastx        :    5.000s:    9.622 GByte/s                       
$ memsum_tight_yzx_fastx        :    5.000s:    6.520 GByte/s                       
$ memsum_tight_zyx              :    5.000s:    2.697 GByte/s                       
$ memsum_tight_yzx              :    5.000s:    2.356 GByte/s                       
$ memsum_tight_zxy              :    5.000s:    0.904 GByte/s                       
$ memsum_tight_xzy              :    5.000s:    0.147 GByte/s                       
$ memsum_tight_yxz              :    5.000s:    0.083 GByte/s                       
$ memsum_tight_xyz              :    5.000s:    0.069 GByte/s                       
$ memsamp1_zyx                  :    5.000s:  365.486 MSamp/s                       
$ memsamp1_yzx                  :    5.000s:  361.526 MSamp/s                       
$ memsamp1_zxy                  :    5.000s:   60.583 MSamp/s                       
$ memsamp1_xzy                  :    5.000s:   58.038 MSamp/s                       
$ memsamp1_yxz                  :    5.000s:   29.270 MSamp/s                       
$ memsamp1_xyz                  :    5.000s:   27.288 MSamp/s                       
$ memsamp1_tight_zyx            :    5.000s:  358.056 MSamp/s                       
$ memsamp1_tight_yzx            :    5.000s:  354.119 MSamp/s                       
$ memsamp1_tight_zxy            :    5.000s:   62.137 MSamp/s                       
$ memsamp1_tight_xzy            :    5.000s:   45.468 MSamp/s                       
$ memsamp1_tight_yxz            :    5.000s:   29.326 MSamp/s                       
$ memsamp1_tight_xyz            :    5.000s:   25.505 MSamp/s                       
$ allocation                    :    5.000s:    6.223 Mega-allocate-and-free/s      
$ file_read                     :    5.001s:    3.362 GByte/s                       

> Single job, multithreaded (physical cores only), normal priority : 
$ trivial1d                     :    5.000s:    5.741 GItems/s                      : x4.024   
$ trivial2d                     :    5.000s:    5.742 GItems/s                      : x4.024   
$ trivial3d                     :    5.000s:    5.740 GItems/s                      : x4.003   
$ compute                       :    5.000s:    3.069 GIter/s                       : x4.025   
$ memsum                        :    5.000s:   38.592 GByte/s                       : x3.024   
$ memcpy                        :    5.001s:   12.578 GByte/s                       : x3.671   
$ memset                        :    5.002s:   20.195 GByte/s                       : x3.782   
$ tlbthrash_read                :    5.000s:  110.439 M4kPage/s                     : x3.808   
$ tlbthrash_copy                :    5.000s:   48.832 M4kPage/s                     : x3.730   
$ tlbthrash_write               :    5.000s:   66.698 M4kPage/s                     : x2.235   
$ memsum_zyx_fastx              :    5.000s:   36.300 GByte/s                       : x3.057   
$ memsum_yzx_fastx              :    5.000s:   29.370 GByte/s                       : x3.397   
$ memsum_zyx                    :    5.000s:   10.750 GByte/s                       : x3.939   
$ memsum_yzx                    :    5.000s:    9.635 GByte/s                       : x3.774   
$ memsum_zxy                    :    5.000s:    3.215 GByte/s                       : x3.621   
$ memsum_xzy                    :    5.000s:    0.933 GByte/s                       : x3.898   
$ memsum_yxz                    :    5.000s:    0.328 GByte/s                       : x3.987   
$ memsum_xyz                    :    5.000s:    0.296 GByte/s                       : x3.865   
$ memsum_tight_zyx_fastx        :    5.000s:   20.824 GByte/s                       : x2.164   
$ memsum_tight_yzx_fastx        :    5.000s:   20.080 GByte/s                       : x3.080   
$ memsum_tight_zyx              :    5.000s:    8.667 GByte/s                       : x3.214   
$ memsum_tight_yzx              :    5.000s:    8.415 GByte/s                       : x3.572   
$ memsum_tight_zxy              :    5.000s:    2.374 GByte/s                       : x2.627   
$ memsum_tight_xzy              :    5.000s:    0.509 GByte/s                       : x3.475   
$ memsum_tight_yxz              :    5.000s:    0.329 GByte/s                       : x3.979   
$ memsum_tight_xyz              :    5.000s:    0.232 GByte/s                       : x3.363   
$ memsamp1_zyx                  :    5.000s: 1457.383 MSamp/s                       : x3.988   
$ memsamp1_yzx                  :    5.000s: 1443.971 MSamp/s                       : x3.994   
$ memsamp1_zxy                  :    5.000s:  239.119 MSamp/s                       : x3.947   
$ memsamp1_xzy                  :    5.000s:  178.671 MSamp/s                       : x3.079   
$ memsamp1_yxz                  :    5.000s:  116.646 MSamp/s                       : x3.985   
$ memsamp1_xyz                  :    5.000s:   95.737 MSamp/s                       : x3.508   
$ memsamp1_tight_zyx            :    5.000s: 1340.506 MSamp/s                       : x3.744   
$ memsamp1_tight_yzx            :    5.000s: 1346.838 MSamp/s                       : x3.803   
$ memsamp1_tight_zxy            :    5.000s:  240.255 MSamp/s                       : x3.867   
$ memsamp1_tight_xzy            :    5.000s:  148.577 MSamp/s                       : x3.268   
$ memsamp1_tight_yxz            :    5.000s:  116.474 MSamp/s                       : x3.972   
$ memsamp1_tight_xyz            :    5.000s:   83.694 MSamp/s                       : x3.282   
$ allocation                    :    5.000s:    7.599 Mega-allocate-and-free/s      : x1.221   
$ file_read                     :    5.001s:   13.320 GByte/s                       : x3.962   

> Single job, multithreaded (all cores), normal priority (multithreaded reference): 
$ trivial1d                     :    5.000s:    5.721 GItems/s                      : x0.996   
$ trivial2d                     :    5.000s:    5.713 GItems/s                      : x0.995   
$ trivial3d                     :    5.020s:    5.712 GItems/s                      : x0.995   
$ compute                       :    5.015s:    3.133 GIter/s                       : x1.021   
$ memsum                        :    5.000s:   40.941 GByte/s                       : x1.061   
$ memcpy                        :    5.001s:   13.858 GByte/s                       : x1.102   
$ memset                        :    5.001s:   15.525 GByte/s                       : x0.769   
$ tlbthrash_read                :    5.000s:  107.608 M4kPage/s                     : x0.974   
$ tlbthrash_copy                :    5.000s:   48.822 M4kPage/s                     : x1.000   
$ tlbthrash_write               :    5.000s:   66.592 M4kPage/s                     : x0.998   
$ memsum_zyx_fastx              :    5.000s:   40.526 GByte/s                       : x1.116   
$ memsum_yzx_fastx              :    5.000s:   35.882 GByte/s                       : x1.222   
$ memsum_zyx                    :    5.016s:   11.177 GByte/s                       : x1.040   
$ memsum_yzx                    :    5.000s:   10.963 GByte/s                       : x1.138   
$ memsum_zxy                    :    5.000s:    0.993 GByte/s                       : x0.309   
$ memsum_xzy                    :    5.000s:    0.712 GByte/s                       : x0.763   
$ memsum_yxz                    :    5.000s:    0.304 GByte/s                       : x0.926   
$ memsum_xyz                    :    5.000s:    0.281 GByte/s                       : x0.949   
$ memsum_tight_zyx_fastx        :    5.000s:   28.939 GByte/s                       : x1.390   
$ memsum_tight_yzx_fastx        :    5.000s:   26.525 GByte/s                       : x1.321   
$ memsum_tight_zyx              :    5.000s:    9.988 GByte/s                       : x1.152   
$ memsum_tight_yzx              :    5.000s:   10.029 GByte/s                       : x1.192   
$ memsum_tight_zxy              :    5.000s:    0.995 GByte/s                       : x0.419   
$ memsum_tight_xzy              :    5.000s:    0.556 GByte/s                       : x1.093   
$ memsum_tight_yxz              :    5.000s:    0.318 GByte/s                       : x0.969   
$ memsum_tight_xyz              :    5.000s:    0.230 GByte/s                       : x0.991   
$ memsamp1_zyx                  :    5.000s: 1761.023 MSamp/s                       : x1.208   
$ memsamp1_yzx                  :    5.000s: 1752.130 MSamp/s                       : x1.213   
$ memsamp1_zxy                  :    5.000s:  233.284 MSamp/s                       : x0.976   
$ memsamp1_xzy                  :    5.000s:  174.389 MSamp/s                       : x0.976   
$ memsamp1_yxz                  :    5.000s:  109.990 MSamp/s                       : x0.943   
$ memsamp1_xyz                  :    5.000s:  101.193 MSamp/s                       : x1.057   
$ memsamp1_tight_zyx            :    5.000s: 1680.294 MSamp/s                       : x1.253   
$ memsamp1_tight_yzx            :    5.000s: 1666.449 MSamp/s                       : x1.237   
$ memsamp1_tight_zxy            :    5.000s:  247.976 MSamp/s                       : x1.032   
$ memsamp1_tight_xzy            :    5.000s:  151.214 MSamp/s                       : x1.018   
$ memsamp1_tight_yxz            :    5.000s:  114.335 MSamp/s                       : x0.982   
$ memsamp1_tight_xyz            :    5.000s:   88.528 MSamp/s                       : x1.058   
$ allocation                    :    5.000s:    8.880 Mega-allocate-and-free/s      : x1.168   
$ file_read                     :    5.001s:   18.104 GByte/s                       : x1.359   
