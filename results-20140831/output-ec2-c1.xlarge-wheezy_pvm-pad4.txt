Number of cores is 8
sizeof(size_t) is 8
sizeof(uint64_t) is 8
Base data size is (zyx) 1024 x 1024 x 1026 x 2 bytes
File I/O test files (256 4MByte files) will be created in /tmp
Initializing data...
...initialized data
Initialized files
Read back files
Testing blocked_range2d...
...wrote blocked_range2d-1.txt, blocked_range2d-32.txt next...
...tested blocked_range2d
> Single job, single-threaded, normal priority (scaling reference): 
$ trivial1d                     :    5.000s:    1.294 GItems/s                      
$ trivial2d                     :    5.000s:    1.268 GItems/s                      
$ trivial3d                     :    5.000s:    1.279 GItems/s                      
$ compute                       :    5.000s:    0.674 GIter/s                       
$ memsum                        :    5.000s:   10.760 GByte/s                       
$ memcpy                        :    5.000s:    3.231 GByte/s                       
$ memset                        :    5.004s:    4.762 GByte/s                       
$ tlbthrash_read                :    5.000s:   33.492 M4kPage/s                     
$ tlbthrash_copy                :    5.000s:   14.569 M4kPage/s                     
$ tlbthrash_write               :    5.000s:   36.790 M4kPage/s                     
$ memsum_zyx_fastx              :    5.000s:   10.485 GByte/s                       
$ memsum_yzx_fastx              :    5.000s:    8.324 GByte/s                       
$ memsum_zyx                    :    5.000s:    2.396 GByte/s                       
$ memsum_yzx                    :    5.000s:    2.301 GByte/s                       
$ memsum_zxy                    :    5.000s:    0.817 GByte/s                       
$ memsum_xzy                    :    5.000s:    0.352 GByte/s                       
$ memsum_yxz                    :    5.000s:    0.119 GByte/s                       
$ memsum_xyz                    :    5.000s:    0.103 GByte/s                       
$ memsum_tight_zyx_fastx        :    5.000s:    8.472 GByte/s                       
$ memsum_tight_yzx_fastx        :    5.000s:    6.211 GByte/s                       
$ memsum_tight_zyx              :    5.000s:    2.435 GByte/s                       
$ memsum_tight_yzx              :    5.000s:    2.079 GByte/s                       
$ memsum_tight_zxy              :    5.000s:    0.820 GByte/s                       
$ memsum_tight_xzy              :    5.000s:    0.162 GByte/s                       
$ memsum_tight_yxz              :    5.000s:    0.121 GByte/s                       
$ memsum_tight_xyz              :    5.000s:    0.089 GByte/s                       
$ memsamp1_zyx                  :    5.000s:  322.615 MSamp/s                       
$ memsamp1_yzx                  :    5.000s:  318.419 MSamp/s                       
$ memsamp1_zxy                  :    5.000s:   92.191 MSamp/s                       
$ memsamp1_xzy                  :    5.000s:   74.753 MSamp/s                       
$ memsamp1_yxz                  :    5.000s:   42.670 MSamp/s                       
$ memsamp1_xyz                  :    5.000s:   32.466 MSamp/s                       
$ memsamp1_tight_zyx            :    5.000s:  316.063 MSamp/s                       
$ memsamp1_tight_yzx            :    5.000s:  320.040 MSamp/s                       
$ memsamp1_tight_zxy            :    5.000s:   92.114 MSamp/s                       
$ memsamp1_tight_xzy            :    5.000s:   46.757 MSamp/s                       
$ memsamp1_tight_yxz            :    5.000s:   44.677 MSamp/s                       
$ memsamp1_tight_xyz            :    5.000s:   30.334 MSamp/s                       
$ allocation                    :    5.000s:    5.610 Mega-allocate-and-free/s      
$ file_read                     :    5.001s:    2.752 GByte/s                       

> Single job, multithreaded (physical cores only), normal priority : 
$ trivial1d                     :    5.000s:    4.380 GItems/s                      : x3.384   
$ trivial2d                     :    5.000s:    3.665 GItems/s                      : x2.890   
$ trivial3d                     :    5.000s:    3.899 GItems/s                      : x3.049   
$ compute                       :    5.000s:    1.993 GIter/s                       : x2.957   
$ memsum                        :    5.000s:   29.587 GByte/s                       : x2.750   
$ memcpy                        :    5.002s:    9.471 GByte/s                       : x2.931   
$ memset                        :    5.003s:   14.453 GByte/s                       : x3.035   
$ tlbthrash_read                :    5.000s:   95.711 M4kPage/s                     : x2.858   
$ tlbthrash_copy                :    5.000s:   43.082 M4kPage/s                     : x2.957   
$ tlbthrash_write               :    5.000s:   61.510 M4kPage/s                     : x1.672   
$ memsum_zyx_fastx              :    5.000s:   23.082 GByte/s                       : x2.201   
$ memsum_yzx_fastx              :    5.000s:   21.261 GByte/s                       : x2.554   
$ memsum_zyx                    :    5.000s:    5.814 GByte/s                       : x2.427   
$ memsum_yzx                    :    5.000s:    5.510 GByte/s                       : x2.395   
$ memsum_zxy                    :    5.000s:    1.169 GByte/s                       : x1.431   
$ memsum_xzy                    :    5.000s:    0.812 GByte/s                       : x2.305   
$ memsum_yxz                    :    5.000s:    0.359 GByte/s                       : x3.022   
$ memsum_xyz                    :    5.000s:    0.287 GByte/s                       : x2.779   
$ memsum_tight_zyx_fastx        :    5.000s:   16.551 GByte/s                       : x1.954   
$ memsum_tight_yzx_fastx        :    5.000s:   17.687 GByte/s                       : x2.847   
$ memsum_tight_zyx              :    5.000s:    5.290 GByte/s                       : x2.172   
$ memsum_tight_yzx              :    5.000s:    5.436 GByte/s                       : x2.615   
$ memsum_tight_zxy              :    5.000s:    1.665 GByte/s                       : x2.031   
$ memsum_tight_xzy              :    5.000s:    0.440 GByte/s                       : x2.710   
$ memsum_tight_yxz              :    5.000s:    0.289 GByte/s                       : x2.393   
$ memsum_tight_xyz              :    5.000s:    0.203 GByte/s                       : x2.273   
$ memsamp1_zyx                  :    5.000s:  780.429 MSamp/s                       : x2.419   
$ memsamp1_yzx                  :    5.000s:  805.447 MSamp/s                       : x2.530   
$ memsamp1_zxy                  :    5.000s:  249.201 MSamp/s                       : x2.703   
$ memsamp1_xzy                  :    5.000s:  144.534 MSamp/s                       : x1.933   
$ memsamp1_yxz                  :    5.000s:  125.043 MSamp/s                       : x2.930   
$ memsamp1_xyz                  :    5.000s:   96.099 MSamp/s                       : x2.960   
$ memsamp1_tight_zyx            :    5.000s:  875.495 MSamp/s                       : x2.770   
$ memsamp1_tight_yzx            :    5.000s:  969.108 MSamp/s                       : x3.028   
$ memsamp1_tight_zxy            :    5.000s:  276.217 MSamp/s                       : x2.999   
$ memsamp1_tight_xzy            :    5.000s:  122.902 MSamp/s                       : x2.629   
$ memsamp1_tight_yxz            :    5.000s:  150.136 MSamp/s                       : x3.360   
$ memsamp1_tight_xyz            :    5.000s:   75.321 MSamp/s                       : x2.483   
$ allocation                    :    5.000s:    6.719 Mega-allocate-and-free/s      : x1.198   
$ file_read                     :    5.001s:    8.551 GByte/s                       : x3.107   

> Single job, multithreaded (all cores), normal priority (multithreaded reference): 
$ trivial1d                     :    5.001s:    4.740 GItems/s                      : x1.082   
$ trivial2d                     :    5.019s:    4.758 GItems/s                      : x1.298   
$ trivial3d                     :    5.016s:    4.732 GItems/s                      : x1.214   
$ compute                       :    5.016s:    2.532 GIter/s                       : x1.271   
$ memsum                        :    5.017s:   37.407 GByte/s                       : x1.264   
$ memcpy                        :    5.002s:   13.347 GByte/s                       : x1.409   
$ memset                        :    5.002s:   16.470 GByte/s                       : x1.140   
$ tlbthrash_read                :    5.000s:  109.304 M4kPage/s                     : x1.142   
$ tlbthrash_copy                :    5.000s:   50.918 M4kPage/s                     : x1.182   
$ tlbthrash_write               :    5.000s:   63.973 M4kPage/s                     : x1.040   
$ memsum_zyx_fastx              :    5.000s:   35.078 GByte/s                       : x1.520   
$ memsum_yzx_fastx              :    5.018s:   30.507 GByte/s                       : x1.435   
$ memsum_zyx                    :    5.016s:    7.236 GByte/s                       : x1.245   
$ memsum_yzx                    :    5.016s:    7.470 GByte/s                       : x1.356   
$ memsum_zxy                    :    5.016s:    1.487 GByte/s                       : x1.272   
$ memsum_xzy                    :    5.016s:    0.782 GByte/s                       : x0.963   
$ memsum_yxz                    :    5.016s:    0.432 GByte/s                       : x1.203   
$ memsum_xyz                    :    5.016s:    0.339 GByte/s                       : x1.182   
$ memsum_tight_zyx_fastx        :    5.016s:   23.343 GByte/s                       : x1.410   
$ memsum_tight_yzx_fastx        :    5.016s:   24.154 GByte/s                       : x1.366   
$ memsum_tight_zyx              :    5.016s:    6.981 GByte/s                       : x1.320   
$ memsum_tight_yzx              :    5.016s:    7.249 GByte/s                       : x1.334   
$ memsum_tight_zxy              :    5.016s:    1.561 GByte/s                       : x0.937   
$ memsum_tight_xzy              :    5.016s:    0.551 GByte/s                       : x1.253   
$ memsum_tight_yxz              :    5.016s:    0.485 GByte/s                       : x1.679   
$ memsum_tight_xyz              :    5.016s:    0.259 GByte/s                       : x1.274   
$ memsamp1_zyx                  :    5.016s: 1143.131 MSamp/s                       : x1.465   
$ memsamp1_yzx                  :    5.016s: 1178.835 MSamp/s                       : x1.464   
$ memsamp1_zxy                  :    5.016s:  305.989 MSamp/s                       : x1.228   
$ memsamp1_xzy                  :    5.016s:  173.731 MSamp/s                       : x1.202   
$ memsamp1_yxz                  :    5.016s:  144.161 MSamp/s                       : x1.153   
$ memsamp1_xyz                  :    5.016s:  104.405 MSamp/s                       : x1.086   
$ memsamp1_tight_zyx            :    5.016s: 1140.979 MSamp/s                       : x1.303   
$ memsamp1_tight_yzx            :    5.016s: 1141.578 MSamp/s                       : x1.178   
$ memsamp1_tight_zxy            :    5.016s:  348.689 MSamp/s                       : x1.262   
$ memsamp1_tight_xzy            :    5.016s:  144.520 MSamp/s                       : x1.176   
$ memsamp1_tight_yxz            :    5.016s:  161.451 MSamp/s                       : x1.075   
$ memsamp1_tight_xyz            :    5.016s:   94.163 MSamp/s                       : x1.250   
$ allocation                    :    5.000s:    8.506 Mega-allocate-and-free/s      : x1.266   
$ file_read                     :    5.000s:   11.387 GByte/s                       : x1.332   
