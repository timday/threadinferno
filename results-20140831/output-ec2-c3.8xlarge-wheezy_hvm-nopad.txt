Number of cores is 32
sizeof(size_t) is 8
sizeof(uint64_t) is 8
Base data size is (zyx) 1024 x 1024 x 1024 x 2 bytes
File I/O test files (256 4MByte files) will be created in /tmp
Initializing data...
...initialized data
Initialized files
Read back files
Testing blocked_range2d...
...wrote blocked_range2d-1.txt, blocked_range2d-32.txt next...
...tested blocked_range2d
> Single job, single-threaded, normal priority (scaling reference): 
$ trivial1d                     :    5.000s:    1.430 GItems/s                      
$ trivial2d                     :    5.000s:    1.434 GItems/s                      
$ trivial3d                     :    5.000s:    1.433 GItems/s                      
$ compute                       :    5.000s:    0.749 GIter/s                       
$ memsum                        :    5.000s:    8.767 GByte/s                       
$ memcpy                        :    5.001s:    3.256 GByte/s                       
$ memset                        :    5.005s:    5.239 GByte/s                       
$ tlbthrash_read                :    5.000s:   25.635 M4kPage/s                     
$ tlbthrash_copy                :    5.000s:   11.608 M4kPage/s                     
$ tlbthrash_write               :    5.000s:   30.867 M4kPage/s                     
$ memsum_zyx_fastx              :    5.000s:    8.667 GByte/s                       
$ memsum_yzx_fastx              :    5.000s:    5.386 GByte/s                       
$ memsum_zyx                    :    5.000s:    2.737 GByte/s                       
$ memsum_yzx                    :    5.000s:    2.200 GByte/s                       
$ memsum_zxy                    :    5.000s:    0.803 GByte/s                       
$ memsum_xzy                    :    5.000s:    0.134 GByte/s                       
$ memsum_yxz                    :    5.000s:    0.055 GByte/s                       
$ memsum_xyz                    :    5.000s:    0.055 GByte/s                       
$ memsum_tight_zyx_fastx        :    5.000s:    6.588 GByte/s                       
$ memsum_tight_yzx_fastx        :    5.000s:    4.004 GByte/s                       
$ memsum_tight_zyx              :    5.000s:    2.701 GByte/s                       
$ memsum_tight_yzx              :    5.000s:    1.918 GByte/s                       
$ memsum_tight_zxy              :    5.000s:    0.828 GByte/s                       
$ memsum_tight_xzy              :    5.000s:    0.133 GByte/s                       
$ memsum_tight_yxz              :    5.000s:    0.055 GByte/s                       
$ memsum_tight_xyz              :    5.000s:    0.054 GByte/s                       
$ memsamp1_zyx                  :    5.000s:  365.448 MSamp/s                       
$ memsamp1_yzx                  :    5.000s:  343.283 MSamp/s                       
$ memsamp1_zxy                  :    5.000s:   66.417 MSamp/s                       
$ memsamp1_xzy                  :    5.000s:   35.758 MSamp/s                       
$ memsamp1_yxz                  :    5.000s:   16.141 MSamp/s                       
$ memsamp1_xyz                  :    5.000s:   15.962 MSamp/s                       
$ memsamp1_tight_zyx            :    5.000s:  354.569 MSamp/s                       
$ memsamp1_tight_yzx            :    5.000s:  328.070 MSamp/s                       
$ memsamp1_tight_zxy            :    5.000s:   68.583 MSamp/s                       
$ memsamp1_tight_xzy            :    5.000s:   35.606 MSamp/s                       
$ memsamp1_tight_yxz            :    5.000s:   16.108 MSamp/s                       
$ memsamp1_tight_xyz            :    5.000s:   15.881 MSamp/s                       
$ allocation                    :    5.000s:    6.403 Mega-allocate-and-free/s      
$ file_read                     :    5.001s:    3.143 GByte/s                       

> Single job, multithreaded (physical cores only), normal priority : 
$ trivial1d                     :    5.000s:   22.695 GItems/s                      : x15.868  
$ trivial2d                     :    5.000s:   22.659 GItems/s                      : x15.797  
$ trivial3d                     :    5.000s:   22.598 GItems/s                      : x15.768  
$ compute                       :    5.000s:   12.301 GIter/s                       : x16.423  
$ memsum                        :    5.000s:   41.829 GByte/s                       : x4.771   
$ memcpy                        :    5.001s:   18.372 GByte/s                       : x5.643   
$ memset                        :    5.001s:   18.983 GByte/s                       : x3.624   
$ tlbthrash_read                :    5.000s:  172.326 M4kPage/s                     : x6.722   
$ tlbthrash_copy                :    5.000s:   62.283 M4kPage/s                     : x5.366   
$ tlbthrash_write               :    5.000s:   62.351 M4kPage/s                     : x2.020   
$ memsum_zyx_fastx              :    5.000s:   42.416 GByte/s                       : x4.894   
$ memsum_yzx_fastx              :    5.000s:   41.082 GByte/s                       : x7.628   
$ memsum_zyx                    :    5.000s:   39.727 GByte/s                       : x14.514  
$ memsum_yzx                    :    5.000s:   36.122 GByte/s                       : x16.416  
$ memsum_zxy                    :    5.000s:    8.139 GByte/s                       : x10.140  
$ memsum_xzy                    :    5.000s:    0.846 GByte/s                       : x6.296   
$ memsum_yxz                    :    5.000s:    0.532 GByte/s                       : x9.681   
$ memsum_xyz                    :    5.000s:    0.607 GByte/s                       : x11.115  
$ memsum_tight_zyx_fastx        :    5.000s:   18.975 GByte/s                       : x2.880   
$ memsum_tight_yzx_fastx        :    5.000s:   16.916 GByte/s                       : x4.225   
$ memsum_tight_zyx              :    5.000s:   18.067 GByte/s                       : x6.689   
$ memsum_tight_yzx              :    5.000s:   14.385 GByte/s                       : x7.499   
$ memsum_tight_zxy              :    5.000s:    4.272 GByte/s                       : x5.157   
$ memsum_tight_xzy              :    5.000s:    0.362 GByte/s                       : x2.725   
$ memsum_tight_yxz              :    5.000s:    0.421 GByte/s                       : x7.688   
$ memsum_tight_xyz              :    5.000s:    0.154 GByte/s                       : x2.836   
$ memsamp1_zyx                  :    5.000s: 5777.707 MSamp/s                       : x15.810  
$ memsamp1_yzx                  :    5.000s: 5595.916 MSamp/s                       : x16.301  
$ memsamp1_zxy                  :    5.000s:  961.773 MSamp/s                       : x14.481  
$ memsamp1_xzy                  :    5.000s:  260.973 MSamp/s                       : x7.298   
$ memsamp1_yxz                  :    5.000s:  136.154 MSamp/s                       : x8.435   
$ memsamp1_xyz                  :    5.000s:  146.950 MSamp/s                       : x9.206   
$ memsamp1_tight_zyx            :    5.000s: 4969.205 MSamp/s                       : x14.015  
$ memsamp1_tight_yzx            :    5.000s: 3076.122 MSamp/s                       : x9.376   
$ memsamp1_tight_zxy            :    5.000s: 1010.561 MSamp/s                       : x14.735  
$ memsamp1_tight_xzy            :    5.000s:  106.526 MSamp/s                       : x2.992   
$ memsamp1_tight_yxz            :    5.000s:  148.535 MSamp/s                       : x9.221   
$ memsamp1_tight_xyz            :    5.000s:   66.227 MSamp/s                       : x4.170   
$ allocation                    :    5.000s:    7.934 Mega-allocate-and-free/s      : x1.239   
$ file_read                     :    5.000s:   38.558 GByte/s                       : x12.269  

> Single job, multithreaded (all cores), normal priority (multithreaded reference): 
$ trivial1d                     :    5.000s:   22.622 GItems/s                      : x0.997   
$ trivial2d                     :    5.013s:   22.546 GItems/s                      : x0.995   
$ trivial3d                     :    5.016s:   22.489 GItems/s                      : x0.995   
$ compute                       :    5.015s:   12.485 GIter/s                       : x1.015   
$ memsum                        :    5.016s:   40.133 GByte/s                       : x0.959   
$ memcpy                        :    5.001s:   17.272 GByte/s                       : x0.940   
$ memset                        :    5.001s:   14.363 GByte/s                       : x0.757   
$ tlbthrash_read                :    5.000s:  171.826 M4kPage/s                     : x0.997   
$ tlbthrash_copy                :    5.000s:   62.005 M4kPage/s                     : x0.996   
$ tlbthrash_write               :    5.000s:   62.145 M4kPage/s                     : x0.997   
$ memsum_zyx_fastx              :    5.000s:   41.980 GByte/s                       : x0.990   
$ memsum_yzx_fastx              :    5.000s:   40.070 GByte/s                       : x0.975   
$ memsum_zyx                    :    5.000s:   41.376 GByte/s                       : x1.041   
$ memsum_yzx                    :    5.013s:   37.964 GByte/s                       : x1.051   
$ memsum_zxy                    :    5.000s:    3.902 GByte/s                       : x0.479   
$ memsum_xzy                    :    5.000s:    1.021 GByte/s                       : x1.207   
$ memsum_yxz                    :    5.019s:    0.547 GByte/s                       : x1.028   
$ memsum_xyz                    :    5.000s:    0.671 GByte/s                       : x1.106   
$ memsum_tight_zyx_fastx        :    5.000s:   27.498 GByte/s                       : x1.449   
$ memsum_tight_yzx_fastx        :    5.015s:   13.144 GByte/s                       : x0.777   
$ memsum_tight_zyx              :    5.016s:   24.803 GByte/s                       : x1.373   
$ memsum_tight_yzx              :    5.016s:   12.076 GByte/s                       : x0.839   
$ memsum_tight_zxy              :    5.016s:    3.646 GByte/s                       : x0.854   
$ memsum_tight_xzy              :    5.016s:    0.359 GByte/s                       : x0.993   
$ memsum_tight_yxz              :    5.016s:    0.556 GByte/s                       : x1.321   
$ memsum_tight_xyz              :    5.020s:    0.177 GByte/s                       : x1.146   
$ memsamp1_zyx                  :    5.016s: 6900.519 MSamp/s                       : x1.194   
$ memsamp1_yzx                  :    5.000s: 6765.759 MSamp/s                       : x1.209   
$ memsamp1_zxy                  :    5.015s:  922.479 MSamp/s                       : x0.959   
$ memsamp1_xzy                  :    5.016s:  346.067 MSamp/s                       : x1.326   
$ memsamp1_yxz                  :    5.016s:  150.474 MSamp/s                       : x1.105   
$ memsamp1_xyz                  :    5.016s:  165.516 MSamp/s                       : x1.126   
$ memsamp1_tight_zyx            :    5.016s: 6243.562 MSamp/s                       : x1.256   
$ memsamp1_tight_yzx            :    5.000s: 2742.386 MSamp/s                       : x0.892   
$ memsamp1_tight_zxy            :    5.000s: 1032.557 MSamp/s                       : x1.022   
$ memsamp1_tight_xzy            :    5.015s:  113.312 MSamp/s                       : x1.064   
$ memsamp1_tight_yxz            :    5.016s:  254.814 MSamp/s                       : x1.716   
$ memsamp1_tight_xyz            :    5.016s:   68.636 MSamp/s                       : x1.036   
$ allocation                    :    5.000s:    6.663 Mega-allocate-and-free/s      : x0.840   
$ file_read                     :    5.001s:   40.798 GByte/s                       : x1.058   
