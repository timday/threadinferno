Number of cores is 32
sizeof(size_t) is 8
sizeof(uint64_t) is 8
Base data size is (zyx) 1024 x 1024 x 1026 x 2 bytes
File I/O test files (256 4MByte files) will be created in /tmp
Initializing data...
...initialized data
Initialized files
Read back files
Testing blocked_range2d...
...wrote blocked_range2d-1.txt, blocked_range2d-32.txt next...
...tested blocked_range2d
> Single job, single-threaded, normal priority (scaling reference): 
$ trivial1d                     :    5.000s:    1.441 GItems/s                      
$ trivial2d                     :    5.000s:    1.441 GItems/s                      
$ trivial3d                     :    5.000s:    1.441 GItems/s                      
$ compute                       :    5.000s:    0.770 GIter/s                       
$ memsum                        :    5.000s:   10.948 GByte/s                       
$ memcpy                        :    5.008s:    3.432 GByte/s                       
$ memset                        :    5.002s:    5.289 GByte/s                       
$ tlbthrash_read                :    5.000s:   28.683 M4kPage/s                     
$ tlbthrash_copy                :    5.000s:   12.885 M4kPage/s                     
$ tlbthrash_write               :    5.000s:   29.251 M4kPage/s                     
$ memsum_zyx_fastx              :    5.000s:   10.234 GByte/s                       
$ memsum_yzx_fastx              :    5.000s:    7.732 GByte/s                       
$ memsum_zyx                    :    5.000s:    2.740 GByte/s                       
$ memsum_yzx                    :    5.000s:    2.506 GByte/s                       
$ memsum_zxy                    :    5.000s:    0.852 GByte/s                       
$ memsum_xzy                    :    5.000s:    0.238 GByte/s                       
$ memsum_yxz                    :    5.000s:    0.080 GByte/s                       
$ memsum_xyz                    :    5.000s:    0.075 GByte/s                       
$ memsum_tight_zyx_fastx        :    5.000s:    8.718 GByte/s                       
$ memsum_tight_yzx_fastx        :    5.000s:    5.865 GByte/s                       
$ memsum_tight_zyx              :    5.000s:    2.710 GByte/s                       
$ memsum_tight_yzx              :    5.000s:    2.309 GByte/s                       
$ memsum_tight_zxy              :    5.000s:    0.886 GByte/s                       
$ memsum_tight_xzy              :    5.000s:    0.144 GByte/s                       
$ memsum_tight_yxz              :    5.000s:    0.081 GByte/s                       
$ memsum_tight_xyz              :    5.000s:    0.068 GByte/s                       
$ memsamp1_zyx                  :    5.000s:  366.385 MSamp/s                       
$ memsamp1_yzx                  :    5.000s:  362.790 MSamp/s                       
$ memsamp1_zxy                  :    5.000s:   59.051 MSamp/s                       
$ memsamp1_xzy                  :    5.000s:   57.164 MSamp/s                       
$ memsamp1_yxz                  :    5.000s:   28.442 MSamp/s                       
$ memsamp1_xyz                  :    5.000s:   27.040 MSamp/s                       
$ memsamp1_tight_zyx            :    5.000s:  358.894 MSamp/s                       
$ memsamp1_tight_yzx            :    5.000s:  355.075 MSamp/s                       
$ memsamp1_tight_zxy            :    5.000s:   59.272 MSamp/s                       
$ memsamp1_tight_xzy            :    5.000s:   40.868 MSamp/s                       
$ memsamp1_tight_yxz            :    5.000s:   28.437 MSamp/s                       
$ memsamp1_tight_xyz            :    5.000s:   25.241 MSamp/s                       
$ allocation                    :    5.000s:    6.341 Mega-allocate-and-free/s      
$ file_read                     :    5.001s:    3.330 GByte/s                       

> Single job, multithreaded (physical cores only), normal priority : 
$ trivial1d                     :    5.000s:   22.707 GItems/s                      : x15.756  
$ trivial2d                     :    5.000s:   22.659 GItems/s                      : x15.724  
$ trivial3d                     :    5.000s:   22.623 GItems/s                      : x15.697  
$ compute                       :    5.000s:   12.291 GIter/s                       : x15.958  
$ memsum                        :    5.000s:   42.920 GByte/s                       : x3.920   
$ memcpy                        :    5.001s:   13.564 GByte/s                       : x3.952   
$ memset                        :    5.001s:   19.318 GByte/s                       : x3.653   
$ tlbthrash_read                :    5.000s:  172.433 M4kPage/s                     : x6.012   
$ tlbthrash_copy                :    5.000s:   57.240 M4kPage/s                     : x4.442   
$ tlbthrash_write               :    5.000s:   62.574 M4kPage/s                     : x2.139   
$ memsum_zyx_fastx              :    5.000s:   42.159 GByte/s                       : x4.119   
$ memsum_yzx_fastx              :    5.000s:   42.230 GByte/s                       : x5.461   
$ memsum_zyx                    :    5.000s:   39.512 GByte/s                       : x14.422  
$ memsum_yzx                    :    5.000s:   35.197 GByte/s                       : x14.044  
$ memsum_zxy                    :    5.000s:    8.618 GByte/s                       : x10.116  
$ memsum_xzy                    :    5.000s:    3.594 GByte/s                       : x15.118  
$ memsum_yxz                    :    5.000s:    1.269 GByte/s                       : x15.780  
$ memsum_xyz                    :    5.000s:    0.995 GByte/s                       : x13.254  
$ memsum_tight_zyx_fastx        :    5.000s:   29.576 GByte/s                       : x3.392   
$ memsum_tight_yzx_fastx        :    5.000s:   27.992 GByte/s                       : x4.773   
$ memsum_tight_zyx              :    5.000s:   26.886 GByte/s                       : x9.921   
$ memsum_tight_yzx              :    5.000s:   23.608 GByte/s                       : x10.226  
$ memsum_tight_zxy              :    5.000s:    5.877 GByte/s                       : x6.635   
$ memsum_tight_xzy              :    5.000s:    1.066 GByte/s                       : x7.409   
$ memsum_tight_yxz              :    5.000s:    1.269 GByte/s                       : x15.717  
$ memsum_tight_xyz              :    5.000s:    0.395 GByte/s                       : x5.812   
$ memsamp1_zyx                  :    5.000s: 5798.145 MSamp/s                       : x15.825  
$ memsamp1_yzx                  :    5.000s: 5776.256 MSamp/s                       : x15.922  
$ memsamp1_zxy                  :    5.000s:  911.328 MSamp/s                       : x15.433  
$ memsamp1_xzy                  :    5.000s:  512.808 MSamp/s                       : x8.971   
$ memsamp1_yxz                  :    5.000s:  447.320 MSamp/s                       : x15.727  
$ memsamp1_xyz                  :    5.000s:  246.748 MSamp/s                       : x9.125   
$ memsamp1_tight_zyx            :    5.000s: 5135.991 MSamp/s                       : x14.311  
$ memsamp1_tight_yzx            :    5.000s: 5171.595 MSamp/s                       : x14.565  
$ memsamp1_tight_zxy            :    5.000s:  926.827 MSamp/s                       : x15.637  
$ memsamp1_tight_xzy            :    5.000s:  213.748 MSamp/s                       : x5.230   
$ memsamp1_tight_yxz            :    5.000s:  448.972 MSamp/s                       : x15.788  
$ memsamp1_tight_xyz            :    5.000s:  111.407 MSamp/s                       : x4.414   
$ allocation                    :    5.000s:    7.985 Mega-allocate-and-free/s      : x1.259   
$ file_read                     :    5.000s:   36.982 GByte/s                       : x11.107  

> Single job, multithreaded (all cores), normal priority (multithreaded reference): 
$ trivial1d                     :    5.000s:   22.598 GItems/s                      : x0.995   
$ trivial2d                     :    5.017s:   22.550 GItems/s                      : x0.995   
$ trivial3d                     :    5.016s:   22.481 GItems/s                      : x0.994   
$ compute                       :    5.016s:   12.486 GIter/s                       : x1.016   
$ memsum                        :    5.000s:   42.947 GByte/s                       : x1.001   
$ memcpy                        :    5.002s:   12.587 GByte/s                       : x0.928   
$ memset                        :    5.001s:   15.277 GByte/s                       : x0.791   
$ tlbthrash_read                :    5.000s:  171.904 M4kPage/s                     : x0.997   
$ tlbthrash_copy                :    5.000s:   57.109 M4kPage/s                     : x0.998   
$ tlbthrash_write               :    5.000s:   62.331 M4kPage/s                     : x0.996   
$ memsum_zyx_fastx              :    5.000s:   41.813 GByte/s                       : x0.992   
$ memsum_yzx_fastx              :    5.000s:   42.290 GByte/s                       : x1.001   
$ memsum_zyx                    :    5.000s:   41.275 GByte/s                       : x1.045   
$ memsum_yzx                    :    5.000s:   40.055 GByte/s                       : x1.138   
$ memsum_zxy                    :    5.000s:    3.819 GByte/s                       : x0.443   
$ memsum_xzy                    :    5.000s:    2.118 GByte/s                       : x0.589   
$ memsum_yxz                    :    5.000s:    1.140 GByte/s                       : x0.898   
$ memsum_xyz                    :    5.000s:    0.967 GByte/s                       : x0.972   
$ memsum_tight_zyx_fastx        :    5.000s:   34.842 GByte/s                       : x1.178   
$ memsum_tight_yzx_fastx        :    5.000s:   31.512 GByte/s                       : x1.126   
$ memsum_tight_zyx              :    5.000s:   30.827 GByte/s                       : x1.147   
$ memsum_tight_yzx              :    5.000s:   28.709 GByte/s                       : x1.216   
$ memsum_tight_zxy              :    5.000s:    3.899 GByte/s                       : x0.664   
$ memsum_tight_xzy              :    5.000s:    1.118 GByte/s                       : x1.048   
$ memsum_tight_yxz              :    5.000s:    1.185 GByte/s                       : x0.934   
$ memsum_tight_xyz              :    5.000s:    0.482 GByte/s                       : x1.219   
$ memsamp1_zyx                  :    5.000s: 6966.880 MSamp/s                       : x1.202   
$ memsamp1_yzx                  :    5.000s: 6937.947 MSamp/s                       : x1.201   
$ memsamp1_zxy                  :    5.000s:  897.540 MSamp/s                       : x0.985   
$ memsamp1_xzy                  :    5.000s:  376.118 MSamp/s                       : x0.733   
$ memsamp1_yxz                  :    5.000s:  411.677 MSamp/s                       : x0.920   
$ memsamp1_xyz                  :    5.013s:  302.638 MSamp/s                       : x1.227   
$ memsamp1_tight_zyx            :    5.000s: 6462.523 MSamp/s                       : x1.258   
$ memsamp1_tight_yzx            :    5.000s: 6478.287 MSamp/s                       : x1.253   
$ memsamp1_tight_zxy            :    5.000s:  959.554 MSamp/s                       : x1.035   
$ memsamp1_tight_xzy            :    5.000s:  207.761 MSamp/s                       : x0.972   
$ memsamp1_tight_yxz            :    5.000s:  433.779 MSamp/s                       : x0.966   
$ memsamp1_tight_xyz            :    5.000s:  135.470 MSamp/s                       : x1.216   
$ allocation                    :    5.000s:    6.817 Mega-allocate-and-free/s      : x0.854   
$ file_read                     :    5.001s:   36.815 GByte/s                       : x0.995   
