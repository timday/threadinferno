Number of cores is 8
sizeof(size_t) is 8
sizeof(uint64_t) is 8
Base data size is (zyx) 1024 x 1024 x 1026 x 2 bytes
File I/O test files (256 4MByte files) will be created in /tmp
Initializing data...
...initialized data
Initialized files
Read back files
Testing blocked_range2d...
...wrote blocked_range2d-1.txt, blocked_range2d-32.txt next...
...tested blocked_range2d
> Single job, single-threaded, normal priority (scaling reference): 
$ trivial1d                     :    5.000s:    1.341 GItems/s                      
$ trivial2d                     :    5.000s:    1.364 GItems/s                      
$ trivial3d                     :    5.000s:    1.364 GItems/s                      
$ compute                       :    5.000s:    0.668 GIter/s                       
$ memsum                        :    5.000s:   11.235 GByte/s                       
$ memcpy                        :    5.004s:    6.195 GByte/s                       
$ memset                        :    5.001s:   11.396 GByte/s                       
$ tlbthrash_read                :    5.000s:   38.671 M4kPage/s                     
$ tlbthrash_copy                :    5.000s:   13.211 M4kPage/s                     
$ tlbthrash_write               :    5.000s:   38.604 M4kPage/s                     
$ memsum_zyx_fastx              :    5.000s:    9.835 GByte/s                       
$ memsum_yzx_fastx              :    5.000s:    7.783 GByte/s                       
$ memsum_zyx                    :    5.000s:    1.743 GByte/s                       
$ memsum_yzx                    :    5.000s:    1.700 GByte/s                       
$ memsum_zxy                    :    5.000s:    0.838 GByte/s                       
$ memsum_xzy                    :    5.000s:    0.241 GByte/s                       
$ memsum_yxz                    :    5.000s:    0.134 GByte/s                       
$ memsum_xyz                    :    5.000s:    0.121 GByte/s                       
$ memsum_tight_zyx_fastx        :    5.000s:    8.771 GByte/s                       
$ memsum_tight_yzx_fastx        :    5.000s:    6.547 GByte/s                       
$ memsum_tight_zyx              :    5.000s:    1.725 GByte/s                       
$ memsum_tight_yzx              :    5.000s:    1.641 GByte/s                       
$ memsum_tight_zxy              :    5.000s:    0.852 GByte/s                       
$ memsum_tight_xzy              :    5.000s:    0.211 GByte/s                       
$ memsum_tight_yxz              :    5.000s:    0.133 GByte/s                       
$ memsum_tight_xyz              :    5.000s:    0.107 GByte/s                       
$ memsamp1_zyx                  :    5.000s:  275.564 MSamp/s                       
$ memsamp1_yzx                  :    5.000s:  273.291 MSamp/s                       
$ memsamp1_zxy                  :    5.000s:   96.302 MSamp/s                       
$ memsamp1_xzy                  :    5.000s:   51.369 MSamp/s                       
$ memsamp1_yxz                  :    5.000s:   50.262 MSamp/s                       
$ memsamp1_xyz                  :    5.000s:   39.679 MSamp/s                       
$ memsamp1_tight_zyx            :    5.000s:  273.415 MSamp/s                       
$ memsamp1_tight_yzx            :    5.000s:  267.167 MSamp/s                       
$ memsamp1_tight_zxy            :    5.000s:   95.930 MSamp/s                       
$ memsamp1_tight_xzy            :    5.000s:   51.409 MSamp/s                       
$ memsamp1_tight_yxz            :    5.000s:   50.296 MSamp/s                       
$ memsamp1_tight_xyz            :    5.000s:   36.810 MSamp/s                       
$ allocation                    :    5.000s:    6.514 Mega-allocate-and-free/s      
$ file_read                     :    5.002s:    3.434 GByte/s                       

> Single job, multithreaded (physical cores only), normal priority : 
$ trivial1d                     :    5.000s:    5.196 GItems/s                      : x3.875   
$ trivial2d                     :    5.000s:    5.200 GItems/s                      : x3.813   
$ trivial3d                     :    5.000s:    5.198 GItems/s                      : x3.811   
$ compute                       :    5.000s:    2.522 GIter/s                       : x3.774   
$ memsum                        :    5.000s:   18.151 GByte/s                       : x1.616   
$ memcpy                        :    5.003s:    8.887 GByte/s                       : x1.434   
$ memset                        :    5.002s:   12.601 GByte/s                       : x1.106   
$ tlbthrash_read                :    5.000s:  139.719 M4kPage/s                     : x3.613   
$ tlbthrash_copy                :    5.000s:   43.698 M4kPage/s                     : x3.308   
$ tlbthrash_write               :    5.000s:  114.696 M4kPage/s                     : x2.971   
$ memsum_zyx_fastx              :    5.000s:   17.444 GByte/s                       : x1.774   
$ memsum_yzx_fastx              :    5.000s:   17.072 GByte/s                       : x2.194   
$ memsum_zyx                    :    5.000s:    6.603 GByte/s                       : x3.788   
$ memsum_yzx                    :    5.000s:    6.366 GByte/s                       : x3.744   
$ memsum_zxy                    :    5.000s:    2.973 GByte/s                       : x3.546   
$ memsum_xzy                    :    5.000s:    0.501 GByte/s                       : x2.085   
$ memsum_yxz                    :    5.000s:    0.501 GByte/s                       : x3.750   
$ memsum_xyz                    :    5.000s:    0.419 GByte/s                       : x3.454   
$ memsum_tight_zyx_fastx        :    5.000s:   15.027 GByte/s                       : x1.713   
$ memsum_tight_yzx_fastx        :    5.000s:   14.277 GByte/s                       : x2.181   
$ memsum_tight_zyx              :    5.000s:    6.044 GByte/s                       : x3.504   
$ memsum_tight_yzx              :    5.000s:    5.995 GByte/s                       : x3.654   
$ memsum_tight_zxy              :    5.000s:    2.674 GByte/s                       : x3.140   
$ memsum_tight_xzy              :    5.000s:    0.468 GByte/s                       : x2.213   
$ memsum_tight_yxz              :    5.000s:    0.503 GByte/s                       : x3.773   
$ memsum_tight_xyz              :    5.000s:    0.307 GByte/s                       : x2.876   
$ memsamp1_zyx                  :    5.000s: 1042.542 MSamp/s                       : x3.783   
$ memsamp1_yzx                  :    5.000s: 1039.242 MSamp/s                       : x3.803   
$ memsamp1_zxy                  :    5.000s:  359.126 MSamp/s                       : x3.729   
$ memsamp1_xzy                  :    5.000s:  136.664 MSamp/s                       : x2.660   
$ memsamp1_yxz                  :    5.000s:  165.897 MSamp/s                       : x3.301   
$ memsamp1_xyz                  :    5.000s:  103.604 MSamp/s                       : x2.611   
$ memsamp1_tight_zyx            :    5.000s: 1006.630 MSamp/s                       : x3.682   
$ memsamp1_tight_yzx            :    5.000s:  996.604 MSamp/s                       : x3.730   
$ memsamp1_tight_zxy            :    5.000s:  356.625 MSamp/s                       : x3.718   
$ memsamp1_tight_xzy            :    5.000s:  112.252 MSamp/s                       : x2.184   
$ memsamp1_tight_yxz            :    5.000s:  187.426 MSamp/s                       : x3.726   
$ memsamp1_tight_xyz            :    5.000s:   90.556 MSamp/s                       : x2.460   
$ allocation                    :    5.000s:   10.258 Mega-allocate-and-free/s      : x1.575   
$ file_read                     :    5.001s:   10.662 GByte/s                       : x3.105   

> Single job, multithreaded (all cores), normal priority (multithreaded reference): 
$ trivial1d                     :    5.000s:    5.182 GItems/s                      : x0.997   
$ trivial2d                     :    5.000s:    5.181 GItems/s                      : x0.996   
$ trivial3d                     :    5.014s:    5.182 GItems/s                      : x0.997   
$ compute                       :    5.016s:    2.841 GIter/s                       : x1.126   
$ memsum                        :    5.001s:   17.465 GByte/s                       : x0.962   
$ memcpy                        :    5.003s:    7.384 GByte/s                       : x0.831   
$ memset                        :    5.002s:   11.771 GByte/s                       : x0.934   
$ tlbthrash_read                :    5.000s:  138.783 M4kPage/s                     : x0.993   
$ tlbthrash_copy                :    5.000s:   43.622 M4kPage/s                     : x0.998   
$ tlbthrash_write               :    5.000s:  114.001 M4kPage/s                     : x0.994   
$ memsum_zyx_fastx              :    5.000s:   17.401 GByte/s                       : x0.998   
$ memsum_yzx_fastx              :    5.021s:   17.027 GByte/s                       : x0.997   
$ memsum_zyx                    :    5.016s:    5.193 GByte/s                       : x0.787   
$ memsum_yzx                    :    5.000s:    5.196 GByte/s                       : x0.816   
$ memsum_zxy                    :    5.000s:    1.639 GByte/s                       : x0.551   
$ memsum_xzy                    :    5.023s:    0.509 GByte/s                       : x1.016   
$ memsum_yxz                    :    5.000s:    0.513 GByte/s                       : x1.022   
$ memsum_xyz                    :    5.000s:    0.385 GByte/s                       : x0.919   
$ memsum_tight_zyx_fastx        :    5.000s:   17.689 GByte/s                       : x1.177   
$ memsum_tight_yzx_fastx        :    5.000s:   15.826 GByte/s                       : x1.108   
$ memsum_tight_zyx              :    5.000s:    5.164 GByte/s                       : x0.854   
$ memsum_tight_yzx              :    5.000s:    5.124 GByte/s                       : x0.855   
$ memsum_tight_zxy              :    5.000s:    1.693 GByte/s                       : x0.633   
$ memsum_tight_xzy              :    5.000s:    0.460 GByte/s                       : x0.984   
$ memsum_tight_yxz              :    5.000s:    0.582 GByte/s                       : x1.156   
$ memsum_tight_xyz              :    5.000s:    0.322 GByte/s                       : x1.048   
$ memsamp1_zyx                  :    5.017s: 1157.240 MSamp/s                       : x1.110   
$ memsamp1_yzx                  :    5.000s: 1153.481 MSamp/s                       : x1.110   
$ memsamp1_zxy                  :    5.000s:  279.058 MSamp/s                       : x0.777   
$ memsamp1_xzy                  :    5.000s:  132.954 MSamp/s                       : x0.973   
$ memsamp1_yxz                  :    5.000s:  152.451 MSamp/s                       : x0.919   
$ memsamp1_xyz                  :    5.000s:   97.564 MSamp/s                       : x0.942   
$ memsamp1_tight_zyx            :    5.000s: 1131.189 MSamp/s                       : x1.124   
$ memsamp1_tight_yzx            :    5.015s: 1123.673 MSamp/s                       : x1.128   
$ memsamp1_tight_zxy            :    5.000s:  414.795 MSamp/s                       : x1.163   
$ memsamp1_tight_xzy            :    5.000s:  118.257 MSamp/s                       : x1.054   
$ memsamp1_tight_yxz            :    5.000s:  200.240 MSamp/s                       : x1.068   
$ memsamp1_tight_xyz            :    5.000s:   94.538 MSamp/s                       : x1.044   
$ allocation                    :    5.000s:   12.440 Mega-allocate-and-free/s      : x1.213   
$ file_read                     :    5.000s:   12.940 GByte/s                       : x1.214   
