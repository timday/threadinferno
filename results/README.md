Hardware
========
All tests on 4 core/8 thread systems.

Native
------
* Namche: [Intel(R) Core(TM) i7 CPU 920 @ 2.67GHz](http://ark.intel.com/products/37147).  Bloomfield Nehalem.  Q4'08.

Peripherally: Captures of TBB `blocked_range2d` range traversal and assigment of tasks to worker threads.

* [blocked_range2d-1.txt](blocked_range2d-1.txt)
* [blocked_range2d-32.txt](blocked_range2d-32.txt)

AWS EC2
-------
EU-West1 (Ireland datacentre).

* c3.2xlarge: [Intel(R) Xeon(R) CPU E5-2680 v2 @ 2.80GHz](http://ark.intel.com/products/75277) - actually a 10 core. Ivy Bridge-EP.

* c1.xlarge: found [Intel(R) Xeon(R) CPU E5506](http://ark.intel.com/products/37096) @ 2.13GHz (launched Q1'09).  Gainestown (Nehalem EP).
    * However have also recently seen newer Intel(R) Xeon(R) CPU E5-2651 v2 @ 1.80GHz (no ark page) - Ivy Bridge-EP.  Actually a 12 core.  No results captured here.

* m2.4xlarge: found [Intel(R) Xeon(R) CPU E5-2665](http://ark.intel.com/products/64597/) (Intel proc.id utility reported 16 threads 8 cores, 2.4GHz; launched Q1'12.  Sandy Bridge?) when booted windows instances; didn't test.
    * However have also recently seen [Intel(R) Xeon(R) CPU X5550 @ 2.67GHz](http://ark.intel.com/products/37106) - Launched Q1'09, a Gainestown Nehalem (Nehalem EP).  4 cores 8 threads.  But no results for that captured here (maybe that was US East).

Linux results
=============
All running Debian/Wheezy (amd64).

Linux on Desktop
----------------
* [output-native-namche_i7-wheezy.txt](output-native-namche_i7-wheezy.txt).

Linux on AWS EC2
----------------
AMIs [here](https://wiki.debian.org/Cloud/AmazonEC2Image/Wheezy).  Used EBS backed instances.  All PVM amd64.

* [output-ec2-c1.xlarge-wheezy.txt](output-ec2-c1.xlarge-wheezy.txt): Debian/Wheezy on E5506.
* [output-ec2-c3.2xlarge-wheezy.txt](output-ec2-c3.2xlarge-wheezy.txt): Debian/Wheezy on E5-2680.

Windows results
===============
Only difference in the source the WINVER makes is use of `THREAD_MODE_BACKGROUND_BEGIN`/`THREAD_MODE_BACKGROUND_END` instead of `THREAD_PRIORITY_LOWEST` / `THREAD_PRIORITY_NORMAL`.

Windows on AWS EC2
------------------
AWS' provided windows instances.  All HVM 64bit.

* [output-ec2-c1.xlarge-2003r2-winver0x0502.txt](output-ec2-c1.xlarge-2003r2-winver0x0502.txt): Windows Server2003R2 64bit on E5506 (running the Server2003 build).
* [output-ec2-c1.xlarge-2012r2.txt](output-ec2-c1.xlarge-2012r2.txt): Windows Server2012R2 64bit on E5506.
* [output-ec2-c1.xlarge-2012r2-winver0x0502.txt](output-ec2-c1.xlarge-2012r2-winver0x0502.txt): Windows Server2003R2 64bit on E5506 (running the Server2003 build).
* [output-ec2-c3.2xlarge-2003r2-winver0x0502.txt](output-ec2-c3.2xlarge-2003r2-winver0x0502.txt): Windows Server2003R2 64bit on E5-2680 (running the Server2003 build obviously).
* [output-ec2-c3.2xlarge-2012r2.txt](output-ec2-c3.2xlarge-2012r2.txt): Windows Server2012R2 64bit on E5-2680.
* [output-ec2-c3.2xlarge-2012r2-winver0x0502.txt](output-ec2-c3.2xlarge-2012r2-winver0x0502.txt): Windows Server2012R2 64bit on E5-2680 running the Server2003 compatible build.
