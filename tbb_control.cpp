#include "tbb_control.h"

#include "tbb_prioritizer.h"

tbb_control::tbb_control()
{}
  
tbb_control::~tbb_control()
{}

void tbb_control::setup(int fg_threads,int bg_threads,bool bg_low_priority) {
  if (_arena) {
    _arena->debug_wait_until_empty();
    _observer.reset();
    _arena.reset();
  }
  sleep(1);
  _sched.terminate();
    sleep(1);
    _sched.initialize(fg_threads+bg_threads);
    if (bg_threads>0) {
      _arena=std::unique_ptr<tbb::task_arena>(new tbb::task_arena(bg_threads,0)); 
      _arena->initialize();
      if (bg_low_priority) {
        _observer=std::unique_ptr<tbb_prioritizer>(new tbb_prioritizer());
      }
    }
    _sched.terminate();
    _sched.initialize(fg_threads);
}

tbb::task_arena& tbb_control::arena() {
  return *_arena;
}
