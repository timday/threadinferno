#ifndef __threadinferno_tbb_control_h__
#define __threadinferno_tbb_control_h__

#include <memory>

#include <tbb/task_arena.h>
#include <tbb/task_scheduler_init.h>
#include <tbb/task_scheduler_observer.h>

class tbb_control {
public:

  tbb_control();
  ~tbb_control();

  void setup(int fg_threads,int bg_threads,bool bg_low_priority);

  tbb::task_arena& arena();

private:
  tbb::task_scheduler_init _sched;
  std::unique_ptr<tbb::task_arena> _arena;
  std::unique_ptr<tbb::task_scheduler_observer> _observer;
};

#endif
