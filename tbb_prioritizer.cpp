#include "tbb_prioritizer.h"

#include <iostream>

#ifdef WIN32

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

namespace {
  const DWORD mainthread_tid=GetCurrentThreadId();

  void set_worker_thread_priority(int priority) {
    const DWORD tid=GetCurrentThreadId();
    if (tid!=mainthread_tid) {
      if (SetThreadPriority(GetCurrentThread(),priority)==0) {
        std::cerr << "Failed to set priority" << priority << std::endl;
      }
    }
  }
  
  const int windows_priority_high=THREAD_PRIORITY_TIME_CRITICAL;
#if (WINVER > 0x0502)
#pragma message("Compiling for WINVER > 0x0502 (post-Server2003)")
  const int windows_priority_low=THREAD_MODE_BACKGROUND_BEGIN;
  const int windows_priority_normal=THREAD_MODE_BACKGROUND_END;
#else
#pragma message("Compiling for WINVER <= 0x0502 (Server2003)")
  const int windows_priority_low=THREAD_PRIORITY_LOWEST;
  const int windows_priority_normal=THREAD_PRIORITY_NORMAL;
#endif
}

void set_worker_thread_priority_low() {
  set_worker_thread_priority(windows_priority_low);
}

void set_worker_thread_priority_normal() {
  set_worker_thread_priority(windows_priority_normal);
}

void set_worker_thread_priority_high() {
  set_worker_thread_priority(windows_priority_high);
}

#else

#include <sys/resource.h>

namespace {

  pid_t gettid() {
    const pid_t tid=syscall(SYS_gettid);
    return tid;
  }
  
  const pid_t mainthread_tid=gettid();

  void set_worker_thread_priority(int priority) {
    const pid_t tid=gettid();
    if (tid!=mainthread_tid) {
      const int current_priority=getpriority(PRIO_PROCESS,tid);
      if (current_priority!=priority) {
        const int r=setpriority(PRIO_PROCESS,tid,priority);
        if (r==-1) std::cout << "[set thread priority " << priority << " failed]";
      }
    }
  }
}

void set_worker_thread_priority_low() {
  set_worker_thread_priority(19);
}

void set_worker_thread_priority_normal() {
  set_worker_thread_priority(0);
}

void set_worker_thread_priority_high() {
  set_worker_thread_priority(-10);
}

#endif

tbb_prioritizer::tbb_prioritizer() {
  observe(true);
}

tbb_prioritizer::~tbb_prioritizer() {
  observe(false);
}

void tbb_prioritizer::on_scheduler_entry(bool worker) {
  set_worker_thread_priority_low();
}

void tbb_prioritizer::on_scheduler_exit(bool worker) {
  set_worker_thread_priority_normal();
}
