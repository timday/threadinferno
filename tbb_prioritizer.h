#ifndef __threadinferno_tbb_prioritizer_h__
#define __threadinferno_tbb_prioritizer_h__

#include <tbb/task_arena.h>
#include <tbb/task_scheduler_observer.h>

extern void set_worker_thread_priority_low();
extern void set_worker_thread_priority_normal();
extern void set_worker_thread_priority_high();

class tbb_prioritizer : public tbb::task_scheduler_observer {
public:
  tbb_prioritizer();
  ~tbb_prioritizer();
  void on_scheduler_entry(bool worker) override;
  void on_scheduler_exit(bool worker) override;
};

#endif
