#include <cstddef>
#include <cstdint>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <map>
#include <memory>
#include <vector>

#include <boost/program_options.hpp>

#include <tbb/tick_count.h>

#include "aborter.h"
#include "job.h"
#include "job_resources.h"
#include "tbb_control.h"

const int duration=5;

std::pair<double,std::string> benchmark_job_single(
  const std::string& name,
  job& j,
  const std::map<std::string,std::pair<double,std::string> > results  
) {
  const aborter abort(duration);
  const tbb::tick_count start=tbb::tick_count::now();;  
  const int64_t ran=j.run(abort);
  const double t=(tbb::tick_count::now()-start).seconds();
  const double rate=double(ran)*j.scale()/t;
  const std::string units=j.units()+"/s";
  std::cout 
    << "$ "
    << std::left << std::setw(30) << name
    << ": " 
    << std::right << std::fixed << std::setprecision(3) << std::setw(8) << t 
    << "s: " 
    << std::right << std::fixed << std::setprecision(3) << std::setw(8) << rate 
    << " " 
    << std::left << std::setw(30) << units;
  if (results.find(name)!=results.end()) {
    std::cout 
      << ": x"
      << std::left << std::fixed << std::setprecision(3) << std::setw(8) << rate/results.at(name).first;
  }
  std::cout << std::endl;
  return std::make_pair(rate,units);
}

void benchmark_job_dual(
  const std::string& name0,
  job& j0,
  const std::string& name1,
  job& j1,
  tbb::task_arena& arena,
  const std::map<std::string,std::pair<double,std::string> > results
) {
  aborter abort(duration);
  uint64_t ran_arena=0;
  const tbb::tick_count start=tbb::tick_count::now();;  
  arena.enqueue([&ran_arena,&abort,&j1](){ran_arena=j1.run(abort);});
  const uint64_t ran=j0.run(abort);
  arena.debug_wait_until_empty();
  const double t=(tbb::tick_count::now()-start).seconds();

  const double rate0=double(ran)*j0.scale()/t;
  const double rate1=double(ran_arena)*j1.scale()/t;
  const double base0=results.at(name0).first;
  const double base1=results.at(name1).first;
  const double eff0=rate0/base0;
  const double eff1=rate1/base1;
  
  std::cout 
    << "$ "
    << std::left << std::setw(30) << name0 
    << " & " 
    << std::left << std::setw(30) << name1 
    << ": " 
    << std::right << std::fixed << std::setprecision(3) << std::setw(8) << t 
    << "s: " 
    << std::right << std::fixed << std::setprecision(3) << std::setw(8) << eff0 
    << " & " 
    << std::right << std::fixed << std::setprecision(3) << std::setw(8) << eff1 
    << ": " 
    << std::right << std::fixed << std::setprecision(3) << std::setw(8) << eff0+eff1 
    << std::endl;
}

typedef std::pair<std::function<std::shared_ptr<job>()>,std::string> job_factory_and_name_t;
typedef std::vector<std::shared_ptr<job_factory_and_name_t> > jobs_t;

const char*const default_files_path=
#ifdef WIN32
"Z:";
#else
"/tmp";
#endif

int main(int argc,char** argv) {

  const int num_cores=tbb::task_scheduler_init::default_num_threads();
  tbb_control tbb_ctrl;

  boost::program_options::options_description options_description("threadinferno options");
  options_description.add_options()
    ("help","Print help message")
    ("basic","Run basic testing only, not job vs. job impact")
    ("workspace",boost::program_options::value<std::string>()->default_value(default_files_path),"Directory for file I/O test files (or last positional argument)");

  boost::program_options::positional_options_description poptions_description;
  poptions_description.add("workspace", -1);

  boost::program_options::variables_map args;
  boost::program_options::store(
    boost::program_options::command_line_parser(argc,argv).options(options_description).positional(poptions_description).run(),
    args
  );
  
  boost::program_options::notify(args);

  if (args.count("help")) {
    std::cout << options_description << std::endl;
    return 1;
  }

  const std::string files_path=args["workspace"].as<std::string>();

  std::cout << "Number of cores is " << num_cores << std::endl;
  std::cout << "sizeof(size_t) is " << sizeof(size_t) << std::endl;
  std::cout << "sizeof(uint64_t) is " << sizeof(uint64_t) << std::endl;
  std::cout << "Base data size is (zyx) " << job::data_size_z << " x " << job::data_size_y << " x " << job::data_size_x << " x " << sizeof(job::data_t) << " bytes" << std::endl;
  std::cout << "File I/O test files (" << job::number_of_files << " " << job::file_size/double(1<<20) << "MByte files) will be created in " << files_path << std::endl;

  job_resources jobrc(files_path,/*3*/1);
  
  jobs_t jobs;
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_trivial1d>(1);},"trivial1d"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_trivial2d>(1);},"trivial2d"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_trivial3d>(1);},"trivial3d"));

  // jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_trivial1d>(64);},"trivial1d_grain64"));
  // jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_trivial2d>(64);},"trivial2d_grain64"));
  // jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_trivial3d>(64);},"trivial3d_grain64"));

  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_compute>();},"compute"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_memsum>();},"memsum"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_memcpy>();},"memcpy"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_memset>();},"memset"));

  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_tlbthrash_read>();},"tlbthrash_read"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_tlbthrash_copy>();},"tlbthrash_copy"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_tlbthrash_write>();},"tlbthrash_write"));

  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_memsum_strided>(2,1,0,true);} ,"memsum_zyx_fastx"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_memsum_strided>(2,0,1,true);} ,"memsum_yzx_fastx"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_memsum_strided>(2,1,0,false);},"memsum_zyx"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_memsum_strided>(2,0,1,false);},"memsum_yzx"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_memsum_strided>(1,2,0,false);},"memsum_zxy"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_memsum_strided>(0,2,1,false);},"memsum_xzy"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_memsum_strided>(1,0,2,false);},"memsum_yxz"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_memsum_strided>(0,1,2,false);},"memsum_xyz"));

  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_memsum_strided_tight>(2,1,0,true);} ,"memsum_tight_zyx_fastx"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_memsum_strided_tight>(2,0,1,true);} ,"memsum_tight_yzx_fastx"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_memsum_strided_tight>(2,1,0,false);},"memsum_tight_zyx"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_memsum_strided_tight>(2,0,1,false);},"memsum_tight_yzx"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_memsum_strided_tight>(1,2,0,false);},"memsum_tight_zxy"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_memsum_strided_tight>(0,2,1,false);},"memsum_tight_xzy"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_memsum_strided_tight>(1,0,2,false);},"memsum_tight_yxz"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_memsum_strided_tight>(0,1,2,false);},"memsum_tight_xyz"));

  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_memsamp_strided>(2,1,0,1);},"memsamp1_zyx"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_memsamp_strided>(2,0,1,1);},"memsamp1_yzx"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_memsamp_strided>(1,2,0,1);},"memsamp1_zxy"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_memsamp_strided>(0,2,1,1);},"memsamp1_xzy"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_memsamp_strided>(1,0,2,1);},"memsamp1_yxz"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_memsamp_strided>(0,1,2,1);},"memsamp1_xyz"));

  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_memsamp_strided_tight>(2,1,0,1);},"memsamp1_tight_zyx"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_memsamp_strided_tight>(2,0,1,1);},"memsamp1_tight_yzx"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_memsamp_strided_tight>(1,2,0,1);},"memsamp1_tight_zxy"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_memsamp_strided_tight>(0,2,1,1);},"memsamp1_tight_xzy"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_memsamp_strided_tight>(1,0,2,1);},"memsamp1_tight_yxz"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_memsamp_strided_tight>(0,1,2,1);},"memsamp1_tight_xyz"));

  /*
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_memsamp_strided>(2,1,0,2);},"memsamp2_zyx"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_memsamp_strided>(2,0,1,2);},"memsamp2_yzx"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_memsamp_strided>(1,2,0,2);},"memsamp2_zxy"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_memsamp_strided>(0,2,1,2);},"memsamp2_xzy"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_memsamp_strided>(1,0,2,2);},"memsamp2_yxz"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_memsamp_strided>(0,1,2,2);},"memsamp2_xyz"));

  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_memsamp_strided_tight>(2,1,0,2);},"memsamp2_tight_zyx"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_memsamp_strided_tight>(2,0,1,2);},"memsamp2_tight_yzx"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_memsamp_strided_tight>(1,2,0,2);},"memsamp2_tight_zxy"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_memsamp_strided_tight>(0,2,1,2);},"memsamp2_tight_xzy"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_memsamp_strided_tight>(1,0,2,2);},"memsamp2_tight_yxz"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_memsamp_strided_tight>(0,1,2,2);},"memsamp2_tight_xyz"));

  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_memsamp_strided>(2,1,0,3);},"memsamp3_zyx"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_memsamp_strided>(2,0,1,3);},"memsamp3_yzx"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_memsamp_strided>(1,2,0,3);},"memsamp3_zxy"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_memsamp_strided>(0,2,1,3);},"memsamp3_xzy"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_memsamp_strided>(1,0,2,3);},"memsamp3_yxz"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_memsamp_strided>(0,1,2,3);},"memsamp3_xyz"));

  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_memsamp_strided_tight>(2,1,0,3);},"memsamp3_tight_zyx"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_memsamp_strided_tight>(2,0,1,3);},"memsamp3_tight_yzx"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_memsamp_strided_tight>(1,2,0,3);},"memsamp3_tight_zxy"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_memsamp_strided_tight>(0,2,1,3);},"memsamp3_tight_xzy"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_memsamp_strided_tight>(1,0,2,3);},"memsamp3_tight_yxz"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_memsamp_strided_tight>(0,1,2,3);},"memsamp3_tight_xyz"));
  */

  /*
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_memsum_3d>(true);},"memsum_3d_fastx"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_memsum_3d>(false);},"memsum_3d"));

  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_smallmem>(    64);},"smallmem_64KByte"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_smallmem>(   128);},"smallmem_128KByte"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_smallmem>(   256);},"smallmem_256KByte"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_smallmem>(   512);},"smallmem_512KByte"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_smallmem>( 1<<10);},"smallmem_1MByte"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_smallmem>( 2<<10);},"smallmem_2MByte"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_smallmem>( 4<<10);},"smallmem_4MByte"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_smallmem>( 8<<10);},"smallmem_8MByte"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_smallmem>(16<<10);},"smallmem_16MByte"));
  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_smallmem>(32<<10);},"smallmem_32MByte"));
  */

  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_alloc>();},"allocation"));

  jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_file_read>();},"file_read"));
  //jobs.push_back(std::make_shared<job_factory_and_name_t>([](){return std::make_shared<job_file_read>();},"file_read_hot"));

  std::cout << "Testing blocked_range2d..." << std::endl;
  {
    std::ofstream("blocked_range2d-1.txt",std::ofstream::out|std::ofstream::trunc)
      << "> Blocked range 2D pattern (grainsize 1):"
      << std::endl
      << job::probe_tbb_blocked_range2d(1);

    std::cout << "...wrote blocked_range2d-1.txt, blocked_range2d-32.txt next..." << std::endl;

    std::ofstream("blocked_range2d-32.txt",std::ofstream::out|std::ofstream::trunc)
      << "> Blocked range 2D pattern (grainsize 32):"
      << std::endl
      << job::probe_tbb_blocked_range2d(32);
  }
  std::cout << "...tested blocked_range2d" << std::endl;

  tbb_ctrl.setup(1,0,false);

  std::map<std::string,std::pair<double,std::string> > results_null;
  std::map<std::string,std::pair<double,std::string> > results_single;
  std::cout << "> Single job, single-threaded, normal priority (scaling reference): " << std::endl;
  for (jobs_t::const_iterator it=jobs.begin();it!=jobs.end();++it) {
    const std::shared_ptr<job> j((*it)->first());
    const std::string name=(*it)->second;
    results_single.insert(std::make_pair(name,benchmark_job_single(name,*j,results_null)));
  }

  std::cout << std::endl;
 
  tbb_ctrl.setup(num_cores/2,0,false);

  std::map<std::string,std::pair<double,std::string> > results_physical;
  std::cout << "> Single job, multithreaded (physical cores only), normal priority : " << std::endl;
  for (jobs_t::const_iterator it=jobs.begin();it!=jobs.end();++it) {
    const std::shared_ptr<job> j((*it)->first());
    const std::string name=(*it)->second;
    results_physical.insert(std::make_pair(name,benchmark_job_single(name,*j,results_single)));
  }

  std::cout << std::endl;
 
  tbb_ctrl.setup(num_cores,0,false);

  std::map<std::string,std::pair<double,std::string> > results_multi;
  std::cout << "> Single job, multithreaded (all cores), normal priority (multithreaded reference): " << std::endl;
  for (jobs_t::const_iterator it=jobs.begin();it!=jobs.end();++it) {
    const std::shared_ptr<job> j((*it)->first());
    const std::string name=(*it)->second;
    results_multi.insert(std::make_pair(name,benchmark_job_single(name,*j,results_physical)));
  }

  if (args.count("basic")) {
    return 0;
  }

  std::cout << std::endl;

  tbb_ctrl.setup(num_cores/2,num_cores/2,false);
  
  std::cout << "> Dual jobs, no oversubscribing, both normal priority:" << std::endl;
  for (jobs_t::const_iterator it0=jobs.begin();it0!=jobs.end();++it0) {
    const std::shared_ptr<job> j0=(*it0)->first();
    const std::string name0=(*it0)->second;
    for (jobs_t::const_iterator it1=jobs.begin();it1!=jobs.end();++it1) {
      const std::shared_ptr<job> j1=(*it1)->first();
      const std::string name1=(*it1)->second;
      benchmark_job_dual(
        name0,
        *j0,
        name1,
        *j1,
        tbb_ctrl.arena(),
        results_multi
      );
    }
  }

  std::cout << std::endl;

  tbb_ctrl.setup(num_cores,num_cores,false);

  std::cout << "> Dual jobs, oversubscribing, both normal priority:" << std::endl;
  for (jobs_t::const_iterator it0=jobs.begin();it0!=jobs.end();++it0) {
    const std::shared_ptr<job> j0=(*it0)->first();
    const std::string name0=(*it0)->second;
    for (jobs_t::const_iterator it1=jobs.begin();it1!=jobs.end();++it1) {
      const std::shared_ptr<job> j1=(*it1)->first();
      const std::string name1=(*it1)->second;
      benchmark_job_dual(
        name0,
        *j0,
        name1,
        *j1,
        tbb_ctrl.arena(),
        results_multi
      );
    }
  }

  std::cout << std::endl;

  tbb_ctrl.setup(num_cores,num_cores,true);

  std::cout << "> Dual jobs, oversubscribing, normal and low priority:" << std::endl;
  for (jobs_t::const_iterator it0=jobs.begin();it0!=jobs.end();++it0) {
    const std::shared_ptr<job> j0=(*it0)->first();
    const std::string name0=(*it0)->second;
    for (jobs_t::const_iterator it1=jobs.begin();it1!=jobs.end();++it1) {
      const std::shared_ptr<job> j1=(*it1)->first();
      const std::string name1=(*it1)->second;
      benchmark_job_dual(
        name0,
        *j0,
        name1,
        *j1,
        tbb_ctrl.arena(),
        results_multi
      );
    }
  }

  return 0;
}
