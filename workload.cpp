#include "workload.h"

#include <array>
#include <fstream>
#include <iostream>
#include <numeric>
#include <utility>

#ifdef __SSE2__
#include <emmintrin.h>
#endif

size_t workload_base::spin(size_t n,const aborter& abort) {
  size_t i;
  for (i=0;i<n && !abort;++i) {}
  return i;
}

std::pair<uint64_t,uint64_t> workload_base::iterate_hailstone(uint64_t v,uint64_t n,const aborter& abort) {
  size_t i;
  for (i=0;i<n && !abort;++i) {
    v=(v&1 ? 3*v+1 : v/2);
  }
  return std::make_pair(v,i);
}

size_t workload_base::memmod(uint64_t* begin,uint64_t* end,const aborter& abort) {
  size_t n=0;
  for (uint64_t* ptr=begin;ptr<end && !abort;++ptr,++n) {
    ++*ptr;
  }
  return n;
}

std::pair<uint8_t,size_t> workload_base::filesum(const char* filename,size_t n,const aborter& abort) {
  const size_t m=n;
  uint8_t r=0;

  std::ifstream in(filename,std::ifstream::in|std::ifstream::binary);
  
  const std::array<size_t,4> buffersizes={32768,4096,512,1};
  std::array<uint8_t,32768> buffer;
  for (std::array<size_t,4>::const_iterator it=buffersizes.begin();it!=buffersizes.end();++it) {
    const size_t buffersize=(*it);
    while (n>=buffersize && !abort) {
      in.read(reinterpret_cast<char*>(&buffer[0]),buffersize);
      r+=std::accumulate(buffer.begin(),buffer.end(),0);
      n-=buffersize;
    }
  }
  in.close();

  if (!in) {
    std::cerr << "Problem with file IO in filesum" << std::endl;
  }
  
  return std::make_pair(r,m-n);
}

template <typename data_t> std::pair<data_t,size_t> workload<data_t>::memsum(const data_t* src,size_t n,const aborter& abort) {
  const size_t m=n;
  data_t r=0;
  while (n>0 && !abort) {
    --n;
    r+=(*src);
    ++src;
  }
  return std::pair<data_t,size_t>(r,m-n);
}

template <> std::pair<uint8_t,size_t> workload<uint8_t>::memsum(const uint8_t* src,size_t n,const aborter& abort) {
  const size_t m=n;

#ifdef __SSE2__

  uint8_t r=0;

  // Get 16-byte aligned
  while (n>0 && !abort && (reinterpret_cast<ptrdiff_t>(src)&15)!=0) {
    --n;
    r+=(*src);
    ++src;
  }

  const __m128i* p=reinterpret_cast<const __m128i*>(src);
  __m128i v=_mm_set_epi8(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
  
  while (n>=4*16 && !abort) {
    n-=4*16;
    v=_mm_add_epi8(v,p[0]);
    v=_mm_add_epi8(v,p[1]);
    v=_mm_add_epi8(v,p[2]);
    v=_mm_add_epi8(v,p[3]);
    p+=4;
  }
  
  while (n>=16 && !abort) {
    n-=16;
    v=_mm_add_epi8(v,*p);
    ++p;
  }
  
  const uint8_t*const s=reinterpret_cast<const uint8_t*>(&v);
  r+=s[0]+s[1]+s[2]+s[3]+s[4]+s[5]+s[6]+s[7]+s[8]+s[9]+s[10]+s[11]+s[12]+s[13]+s[14]+s[15];  // TODO: Use _mm_hadd_epi8 (two arguments; what's the deal?)
  
  const uint8_t* q=reinterpret_cast<const uint8_t*>(p);
  
  while (n>0 && !abort) {
    --n;
    r+=(*q);
    ++q;
  }
  return std::pair<uint8_t,size_t>(r,m-n);

#else

  uint8_t r=0;
  while (n>0 && !abort) {
    --n;
    r+=(*src);
    ++src;
  }
  return std::pair<uint8_t,size_t>(r,m-n);

#endif
}

template <> std::pair<uint16_t,size_t> workload<uint16_t>::memsum(const uint16_t* src,size_t n,const aborter& abort) {
  const size_t m=n;

#ifdef __SSE2__

  uint16_t r=0;

  // Get 16-byte aligned
  while (n>0 && !abort && (reinterpret_cast<ptrdiff_t>(src)&15)!=0) {
    --n;
    r+=(*src);
    ++src;
  }

  const __m128i* p=reinterpret_cast<const __m128i*>(src);
  __m128i v=_mm_set_epi16(0,0,0,0,0,0,0,0);
  
  while (n>=4*8 && !abort) {
    n-=4*8;
    v=_mm_add_epi16(v,p[0]);
    v=_mm_add_epi16(v,p[1]);
    v=_mm_add_epi16(v,p[2]);
    v=_mm_add_epi16(v,p[3]);
    p+=4;
  }
  
  while (n>=8 && !abort) {
    n-=8;
    v=_mm_add_epi16(v,*p);
    ++p;
  }
  
  const uint16_t*const s=reinterpret_cast<const uint16_t*>(&v);
  r+=s[0]+s[1]+s[2]+s[3]+s[4]+s[5]+s[6]+s[7];
  
  const uint16_t* q=reinterpret_cast<const uint16_t*>(p);
  
  while (n>0 && !abort) {
    --n;
    r+=(*q);
    ++q;
  }
  return std::pair<uint16_t,size_t>(r,m-n);

#else

  uint16_t r=0;
  while (n>0 && !abort) {
    --n;
    r+=(*src);
    ++src;
  }
  return std::pair<uint16_t,size_t>(r,m-n);

#endif
}

template <typename data_t> std::pair<data_t,size_t> workload<data_t>::memsum_strided(const data_t* src,size_t n,size_t stride,const aborter& abort) {
  data_t r=0;
  size_t m=n;
  while (n>0 && !abort) {
    n--;
    r+=*src;
    src+=stride;
  }
  return std::pair<data_t,size_t>(r,m-n);
}

template <typename data_t> std::pair<data_t,size_t> workload<data_t>::memsamp_strided(const data_t* src,size_t n,size_t stride0,size_t d,size_t stride1,size_t stride2,const aborter& abort) {
  switch (d) {
  case 1:
    return _memsamp_strided<1>(src,n,stride0,stride1,stride2,abort);
  case 2:
    return _memsamp_strided<2>(src,n,stride0,stride1,stride2,abort);
  case 3:
    return _memsamp_strided<2>(src,n,stride0,stride1,stride2,abort);
  default:
    throw std::logic_error("Unsupported d in memsamp strided");
  }
}

template <typename data_t> template<int D> std::pair<data_t,size_t> workload<data_t>::_memsamp_strided(const data_t* src,size_t n,size_t stride0,size_t stride1,size_t stride2,const aborter& abort) {
  data_t r=0;
  size_t m=n;
  while (n>0 && !abort) {
    n--;
    for (size_t d2=0;d2<=D;++d2) {
      for (size_t d1=0;d1<=D;++d1) {
        for (size_t d0=0;d0<=D;++d0) {
          r+=*(src+d2*stride2+d1*stride1+d0*stride0);
        }
      }
    }
    src+=stride0;
  }
  return std::pair<data_t,size_t>(r,m-n);  
}

template std::pair<uint8_t,size_t> workload<uint8_t>::memsum(const uint8_t*,size_t,const aborter&);
template std::pair<uint8_t,size_t> workload<uint8_t>::memsum_strided(const uint8_t*,size_t,size_t,const aborter&);
template std::pair<uint8_t,size_t> workload<uint8_t>::memsamp_strided(const uint8_t*,size_t,size_t,size_t,size_t,size_t,const aborter&);

template std::pair<uint16_t,size_t> workload<uint16_t>::memsum(const uint16_t*,size_t,const aborter&);
template std::pair<uint16_t,size_t> workload<uint16_t>::memsum_strided(const uint16_t*,size_t,size_t,const aborter&);
template std::pair<uint16_t,size_t> workload<uint16_t>::memsamp_strided(const uint16_t*,size_t,size_t,size_t,size_t,size_t,const aborter&);
