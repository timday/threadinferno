#ifndef __threadinferno_workload_h__
#define __threadinferno_workload_h__

#include <cstddef>
#include <cstdint>
#include <utility>

#include "aborter.h"

struct workload_base {
  static size_t spin(size_t n,const aborter& abort);

  static std::pair<uint64_t,uint64_t> iterate_hailstone(uint64_t v,uint64_t n,const aborter& abort);

  static size_t memmod(uint64_t* begin,uint64_t* end,const aborter& abort);

  static std::pair<uint8_t,size_t> filesum(const char* filename,size_t n,const aborter& abort);
};

template <typename data_t> struct workload : public workload_base {

  static std::pair<data_t,size_t> memsum(const data_t* src,size_t n,const aborter& abort);
  static std::pair<data_t,size_t> memsum_strided(const data_t* src,size_t n,size_t stride,const aborter& abort);
  
  static std::pair<data_t,size_t> memsamp_strided(const data_t* src,size_t n,size_t stride0,size_t d,size_t stride1,size_t stride2,const aborter& abort);
  
private:
  template <int D> static std::pair<data_t,size_t> _memsamp_strided(const data_t* src,size_t n,size_t stride0,size_t stride1,size_t stride2,const aborter& abort);
};

#endif
